const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
var path = require('path');


require('dotenv').config();


// This will be our application entry. We'll setup our server here.
const http = require('http');

// Set up the express app
const app = express();
//Cors
// var corsOptions = {
//   origin: "http://localhost:8081"
// };
app.use(cors(/*corsOptions*/));

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    next();
});
// Log requests to the console.
app.use(logger('dev'));

// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/public', express.static(path.resolve('./public')));

require('./routes/dashboard.routes')(app);
require('./routes/user.routes')(app);

//Models
const port = parseInt(process.env.PORT, 10) || 8000;
const models = require('./models');
models.sequelize.sync({ alter: false}).then(() => {
    console.log("Drop and re-sync db.  "+ port);
});

app.set('port', port);
const server = http.createServer(app);
server.listen(port, () => {
    console.log(`Server is running on port ${port}.`);
});
module.exports = app;
