let bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const db = require("../models");
const Admin = db.Admin;
const Student = db.Student;
const Role = db.Role;

MealAndCategoryManagement = (req, res, next) => {
    for (let i = 0; i < req.permissions.length; i++) {
        if (req.permissions[i].name === "mealAndCategory-management")
            return next();
    }
    res.status(403).send({
        message: "You don't have the permission!"
    });
};

CouponManagement = (req, res, next) => {
    for (let i = 0; i < req.permissions.length; i++) {
        if (req.permissions[i].name === "coupon-management")
            return next();
    }
    res.status(403).send({
        message: "You don't have the permission!"
    });
};

OfferManagement = (req, res, next) => {
    for (let i = 0; i < req.permissions.length; i++) {
        if (req.permissions[i].name === "offer-management")
            return next();
    }
    res.status(403).send({
        message: "You don't have the permission!"
    });
};

OrderManagement = (req, res, next) => {
    for (let i = 0; i < req.permissions.length; i++) {
        if (req.permissions[i].name === "order-management")
            return next();
    }
    res.status(403).send({
        message: "You don't have the permission!"
    });
};

DriverManagement = (req, res, next) => {
    for (let i = 0; i < req.permissions.length; i++) {
        if (req.permissions[i].name === "driver-management")
            return next();
    }
    res.status(403).send({
        message: "You don't have the permission!"
    });
};

BranchManagement = (req, res, next) => {
    for (let i = 0; i < req.permissions.length; i++) {
        if (req.permissions[i].name === "branch-management")
            return next();
    }
    res.status(403).send({
        message: "You don't have the permission!"
    });
};

AgentManagement = (req, res, next) => {
    for (let i = 0; i < req.permissions.length; i++) {
        if (req.permissions[i].name === "agent-management")
            return next();
    }
    res.status(403).send({
        message: "You don't have the permission!"
    });
};

AdminManagement = (req, res, next) => {
    for (let i = 0; i < req.permissions.length; i++) {
        if (req.permissions[i].name === "admin-management")
            return next();
    }
    res.status(403).send({
        message: "You don't have the permission!"
    });
};

PermissionManagement = (req, res, next) => {
    for (let i = 0; i < req.permissions.length; i++) {
        if (req.permissions[i].name === "permission-management")
            return next();
    }
    res.status(403).send({
        message: "You don't have the permission!"
    });
};

CostumerManagement = (req, res, next) => {
    for (let i = 0; i < req.permissions.length; i++) {
        if (req.permissions[i].name === "costumer-management")
            return next();
    }
    res.status(403).send({
        message: "You don't have the permission!"
    });
};

verifyToken = (req, res, next) => {
    let token = req.headers.authorization;
    if (!token) {
        return res.status(403).send({
            status: 403,
            message: "No token provided!"
        });
    }

    jwt.verify(token, config.secret, async (err, decoded) => {
        if (err) {
            console.log(err);
            return res.status(404).send({message: "token dose not match any user"});
        } else {
            req.userId = decoded.id;
            Admin.findByPk(req.userId, {include: Role})
                .then(user => {
                    if (!user) {
                        return res.status(401).send({message: "not authorized", status: 401});
                    }
                    if (!user.isActive)
                        return res.status(401).send({message: "you are blocked", status: 403});
                    req.username = user.username;
                    req.role = user.Role.role;
                    req.roleId = user.Role.id;
                    req.departmentId = user.departmentId;
                    next();
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).send({message: err.message})
                });
        }
    });
};

verifyStudentToken = (req, res, next) => {

    console.log("verifyStudentToken");
    let token = req.headers.authorization;
    console.log(token)
    if (!token) {
        return res.status(403).send({
            status: 403,
            message: "No token provided!"
        });
    }

    jwt.verify(token, config.secret, async (err, decoded) => {
        if (err) {
            return res.status(500).send({message: "error while validate token", status: 500});
        }
        req.userId = decoded.id;
        Student.findByPk(req.userId)
            .then(user => {
                if (!user) {
                    return res.status(401).send({message: "not authorized", status: 401})
                }
                req.username = user.username;
                req.fcmToken = user.fcmToken;
                next();
            });
    });
};

verifyDriverToken = (req, res, next) => {

    let token = req.headers.authorization;
    if (!token) {
        return res.status(403).send({
            status: 403,
            message: "No token provided!"
        });
    }

    jwt.verify(token, config.secret, async (err, decoded) => {
        if (err) {
            return res.status(500).send({message: "error while validate token", status: 500});
        }
        req.driverId = decoded.id;
        Driver.findByPk(req.driverId)
            .then(driver => {
                if (!driver) {
                    return res.status(401).send({message: "not authorized", status: 401})
                }
                req.username = driver.username;
                req.restaurantId = driver.restaurantId;
                next();
            });
    });
};

studentValidate = (req, res, next) => {
    console.log("studentValidate");
    Student.findByPk(req.userId)
        .then(user => {
            if (!user.isActive) {
                return res.status(403).send({message: "invalid user", status: 403})
            }
            next();
        })
};

DriverValidate = (req, res, next) => {
    Driver.findByPk(req.driverId)
        .then(user => {
            if (!user.isActive) {
                return res.status(403).send({message: "invalid user", status: 403})
            }
            next();
        })
};

isAffairsSuperAdmin = (req, res, next) => {
    if (req.role === "Affairs-SuperAdmin") {
        next();
    } else {
        res.status(403).send({
            message: "Require Affairs-SuperAdmin Role!"
        });
    }
};

isExaminationSuperAdmin = (req, res, next) => {
    if (req.role === "Examination-SuperAdmin") {
        next();
    } else {
        res.status(403).send({
            message: "Require Examination-SuperAdmin Role!"
        });
    }
};

isAffairsAdmin = (req, res, next) => {
    if (req.role === "Affairs-Admin"||req.role === 'Affairs-SuperAdmin') {
        next();
    } else {
        res.status(403).send({
            message: "Require Affairs-Admin Role!"
        });
    }
};

isExaminationAdmin = (req, res, next) => {
    if (req.role === "Examination-Admin"||req.role === "Examination-SuperAdmin") {
        next();
    } else {
        res.status(403).send({
            message: "Require Examination-Admin Role!"
        });
    }
};

isAgent = (req, res, next) => {
    if (req.role === "agent" || req.role === "admin") {
        next();
    } else {
        res.status(403).send({
            message: "Require Agent Role!"
        });
    }
};

checkToken = (req, res) => {
    let token = req.headers.authorization;
    if (!token) {
        return res.status(403).send({
            status: 403,
            message: "No token provided!"
        });
    }

    jwt.verify(token, config.secret, async (err, decoded) => {
        if (err) {
            return res.status(401).send({message: "error while validate token", status: 500});
        }

        res.send({message: "valid token", status: 200})
    });

}

checkActualStudent = (req, res, next) => {
    Student.findByPk(req.userId)
        .then(student => {
            let passwordIsValid = bcrypt.compareSync(
                req.body.password,
                student.password
            );
            if (!passwordIsValid) {
                return res.status(401).send({
                    message: "Invalid Password!"
                });
            }
            next();
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message,status:500})
        })
};

isHeadOfDepartment = (req,res,next) => {
    if (req.role === "Head-Of-Department") {
        next();
    } else {
        res.status(403).send({
            message: "Require Head-Of-Department Role!"
        });
    }
};

isDepartmentEmployee = (req,res,next) => {
    if (req.role === "Head-Of-Department"||req.role === "Department-Employee") {
        next();
    } else {
        res.status(403).send({
            message: "Require Head-Of-Department or Department-Employee Role!"
        });
    }
};

isHeadOfDepartmentOrExaminationAdmin = (req,res,next) => {
    if (req.role === "Head-Of-Department"||req.role === "Examination-Admin"||req.role === "Examination-SuperAdmin") {
        next();
    } else {
        res.status(403).send({
            message: "Require Head-Of-Department or Examination-SuperAdmin or Examination-Admin Role!"
        });
    }
};
const authJwt = {
    verifyToken: verifyToken,
    verifyStudentToken: verifyStudentToken,
    verifyDriverToken: verifyDriverToken,
    isAffairsSuperAdmin: isAffairsSuperAdmin,
    isExaminationSuperAdmin: isExaminationSuperAdmin,
    isAffairsAdmin: isAffairsAdmin,
    isHeadOfDepartment: isHeadOfDepartment,
    isDepartmentEmployee: isDepartmentEmployee,
    isHeadOfDepartmentOrExaminationAdmin: isHeadOfDepartmentOrExaminationAdmin,
    isExaminationAdmin: isExaminationAdmin,
    isAgent: isAgent,
    MealAndCategoryManagement: MealAndCategoryManagement,
    CouponManagement: CouponManagement,
    OfferManagement: OfferManagement,
    OrderManagement: OrderManagement,
    DriverManagement: DriverManagement,
    BranchManagement: BranchManagement,
    AgentManagement: AgentManagement,
    AdminManagement: AdminManagement,
    PermissionManagement: PermissionManagement,
    CostumerManagement: CostumerManagement,
    studentValidate: studentValidate,
    DriverValidate: DriverValidate,
    checkToken: checkToken,
    checkActualStudent:checkActualStudent
};
module.exports = authJwt;
