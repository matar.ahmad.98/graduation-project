let fs = require('fs');
let router = require("express").Router();
let AdminController = require("../controller/admin.controller");
let AuthController = require("../controller/auth.controller");
let Middleware = require("../middleware/authJwt");
let FRegController = require("../controller/freg.controller");
let ARegController = require("../controller/areg.controller");
let ObjectionController = require("../controller/objection.controller");
let MilitaryDeferment = require("../controller/militaryDeferment.controller");
let StudentCareerController = require("../controller/studentCareer.controller");
let StudentController = require("../controller/student.controller");
let Firebase = require("../controller/firebase.controller");

let multer  = require('multer');
let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        fs.mkdirSync(__dirname+`/../public/AReg/${req.username}/FrontID`, { recursive: true });
        fs.mkdirSync(__dirname+`/../public/AReg/${req.username}/BackID`, { recursive: true });
        if(file.fieldname==="BackID")
        {
            cb(null, __dirname+`/../public/AReg/${req.username}/BackID/`);
        }
        else if(file.fieldname==="FrontID")
        {
            cb(null, __dirname+`/../public/AReg/${req.username}/FrontID/`);
        }

    },
    filename: function (req, file, cb) {
        if(file.fieldname==="BackID")
        {
            req.BackID =  Date.now()+ '-' + file.originalname;
            cb(null, Date.now()+ '-' + file.originalname)
        }
        else if(file.fieldname==="FrontID")
        {
            req.FrontID =  Date.now()+ '-' + file.originalname;
            cb(null, Date.now()+ '-' + file.originalname)
        }
    }
});

let storageFReg = multer.diskStorage({
    destination: function (req, file, cb) {
        console.log(2);
        fs.mkdirSync(__dirname+`/../public/FReg/${req.body.username}/PersonalPic`, { recursive: true });
        fs.mkdirSync(__dirname+`/../public/FReg/${req.body.username}/Certificate`, { recursive: true });
        fs.mkdirSync(__dirname+`/../public/FReg/${req.body.username}/FrontID`, { recursive: true });
        fs.mkdirSync(__dirname+`/../public/FReg/${req.body.username}/BackID`, { recursive: true });
        if(file.fieldname==="PersonalPic")
        {
            console.log(3)
            cb(null, __dirname+`/../public/FReg/${req.body.username}/PersonalPic/`)
        }
        else if(file.fieldname==="Certificate")
        {
            console.log(4)
            cb(null, __dirname+`/../public/FReg/${req.body.username}/Certificate/`);
        }
        else if(file.fieldname==="FrontID")
        {
            console.log(5)
            cb(null, __dirname+`/../public/FReg/${req.body.username}/FrontID/`)
        }
        else if(file.fieldname==="BackID")
        {
            cb(null, __dirname+`/../public/FReg/${req.body.username}/BackID/`)
        }
    },
    filename: function (req, file, cb) {
        console.log(10)
        if(file.fieldname==="PersonalPic")
        {
            req.PersonalPic =  Date.now()+ '-' + file.originalname;
            cb(null, Date.now()+ '-' + file.originalname)
        }
        else if(file.fieldname==="Certificate")
        {
            req.Certificate =  Date.now()+ '-' + file.originalname;
            cb(null, Date.now()+ '-' + file.originalname)
        }
        else if(file.fieldname==="FrontID")
        {
            req.FrontID =  Date.now()+ '-' + file.originalname;
            cb(null, Date.now()+ '-' + file.originalname)
        }
        else if(file.fieldname==="BackID")
        {
            req.BackID =  Date.now()+ '-' + file.originalname;
            cb(null, Date.now()+ '-' + file.originalname)
        }
    }
});

let uploadAReg = multer({ storage: storage });
let uploadFReg = multer({ storage: storageFReg });
// let uploadObjection = multer({ storage: storageObjection });
// let uploadMilitaryDeferment = multer({ storage: storageMilitary });
// let uploadStudentCareer = multer({ storage: storageCareer });
module.exports = (app) => {

    router.post("/signIn",
        StudentController.SignIn);

    router.post("/FReg",
        [uploadFReg.fields([{ name: 'FrontID', maxCount: 1 },{name:'BackID', maxCount: 1 }, { name: 'Certificate', maxCount: 1 },{name:"PersonalPic",maxCount:1}])],
        FRegController.create);

    router.post("/AReg",
        [Middleware.verifyStudentToken,Middleware.studentValidate,Middleware.checkActualStudent/*,uploadAReg.fields([{name:"FrontID",maxCount: 1},{name:"BackID",maxCount: 1}])*/],
        ARegController.create);

    router.post("/objection",
        [Middleware.verifyStudentToken,Middleware.studentValidate,Middleware.checkActualStudent],
        ObjectionController.create);

    router.post("/militaryDeferment",
        [Middleware.verifyStudentToken,Middleware.studentValidate,Middleware.checkActualStudent],
        MilitaryDeferment.create);

    router.post("/studentCareer",
        [Middleware.verifyStudentToken,Middleware.studentValidate],
        StudentCareerController.create);

    router.get("/getDepartments",
        StudentController.GetDepartments);

    router.get("/getForeignLanguages",
        StudentController.GetForeignLanguages);

    router.get("/getMilitaryDepartments",
        StudentController.GetMilitaryDepartments);

    router.put("/updateFcmToken",
        [Middleware.verifyStudentToken,Middleware.studentValidate],
        StudentController.UpdateFcmToken);

    router.get("/getObjectionSubjects",
        [Middleware.verifyStudentToken,Middleware.studentValidate],
        ObjectionController.GetObjectionSubjects);

    router.get("/getStudentResults",
        [Middleware.verifyStudentToken],
        StudentController.GetStudentResults);

    router.get("/getStudentProfile",
        [Middleware.verifyStudentToken],
        StudentController.GetStudentProfile);

    router.put("/updateStudentProfile",
        [Middleware.verifyStudentToken],
        StudentController.UpdateStudentProfile);

    router.post("/getMarks",
        [Middleware.verifyStudentToken],
        StudentController.GetMarks);

    router.get("/getMarksStuff",
        [Middleware.verifyStudentToken],
        StudentController.GetMarksStuff);


    router.post("/test",
        (req,res)=>{
            console.log(req.body)
            Firebase.send(req.body.token);
        })
    app.use('/student', router);
};