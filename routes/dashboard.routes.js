let router = require("express").Router();
let multer = require("multer");
let AdminController = require("../controller/admin.controller");
let AuthController = require("../controller/auth.controller");
let FRegController = require("../controller/freg.controller");
let ARegController = require("../controller/areg.controller");
let ObjectionController = require("../controller/objection.controller");
let MilitaryDeferment = require("../controller/militaryDeferment.controller");
let StudentCareerController = require("../controller/studentCareer.controller");
let ExamController = require("../controller/exam.controller");
let AffAdminController = require("../controller/affAdmin.controller");
let AcademicAdmin = require("../controller/academicAdmin.controller");
let StudentController = require("../controller/student.controller");
let ExaminationAdminController = require("../controller/examinationAdmin.controller");
let ConversationController = require("../controller/conversation.controller");
let Middleware = require("../middleware/authJwt");


let CsvStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        // console.log(file);
        cb(null, 'CsvFile/')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
});
let uploadMealsCsv = multer({storage: CsvStorage});

module.exports = (app) => {

    router.post("/signIn",
        AuthController.AdminSignIn);

    router.post("/signUpAffAdmin",
        [Middleware.verifyToken, Middleware.isAffairsSuperAdmin],
        AdminController.AdminAffSignUp);

    router.post("/signUpExaAdmin",
        [Middleware.verifyToken, Middleware.isExaminationSuperAdmin],
        AdminController.AdminExaSignUp);

    router.get("/affAdmin",
        [Middleware.verifyToken, Middleware.isAffairsSuperAdmin],
        AdminController.findAllAffairAdmins);

    router.get("/exaAdmin",
        [Middleware.verifyToken, Middleware.isExaminationSuperAdmin],
        AdminController.findAllExaminationAdmins);

    router.get("/FReg",
        [Middleware.verifyToken, Middleware.isAffairsAdmin],
        FRegController.findAll);

    router.get("/AReg",
        [Middleware.verifyToken, Middleware.isAffairsAdmin],
        ARegController.findAll);

    router.get("/objection",
        [Middleware.verifyToken, Middleware.isAffairsAdmin],
        ObjectionController.findAll);

    router.get("/studentCareer",
        [Middleware.verifyToken, Middleware.isAffairsAdmin],
        StudentCareerController.findAll);

    router.get("/militaryDeferment",
        [Middleware.verifyToken, Middleware.isAffairsAdmin],
        MilitaryDeferment.findAll);

    router.post("/updateRequestStatus",
        [Middleware.verifyToken, Middleware.isAffairsAdmin],
        MilitaryDeferment.UpdateRequestStatus);

    router.post("/sendStdResults",
        [Middleware.verifyToken, Middleware.isExaminationAdmin],
        ExamController.sendStdResults);

    router.get("/pendingStudent",
        [Middleware.verifyToken, Middleware.isAffairsAdmin],
        AffAdminController.getPendingStudents);

    router.post("/approvePendingStudent",
        [Middleware.verifyToken, Middleware.isAffairsAdmin],
        AffAdminController.ApprovePendingStudent);

    router.post("/rejectPendingStudent",
        [Middleware.verifyToken, Middleware.isAffairsAdmin],
        AffAdminController.RejectPendingStudent);

    router.get("/getEmployees",
        [Middleware.verifyToken],
        AcademicAdmin.getEmployees);

    router.post("/createEmployee",
        [Middleware.verifyToken],
        AcademicAdmin.CreateEmployee);

    router.get("/getProfessors",
        [Middleware.verifyToken, Middleware.isDepartmentEmployee],
        AcademicAdmin.GetProfessors);

    router.get("/getSubject",
        [Middleware.verifyToken, Middleware.isDepartmentEmployee],
        AcademicAdmin.GetSubjects);

    router.get("/getAllSubject",
        [Middleware.verifyToken],
        AcademicAdmin.GetAllSubjects);

    router.post("/linkSubjectWithProfessor",
        [Middleware.verifyToken, Middleware.isHeadOfDepartment],
        AcademicAdmin.LinkSubjectWithProfessor);

    router.put("/updateLinkSubjectWithProfessor",
        [Middleware.verifyToken, Middleware.isHeadOfDepartment],
        AcademicAdmin.UpdateLinkSubjectWithProfessor);

    router.put("/deleteSubjectProfessor",
        [Middleware.verifyToken, Middleware.isHeadOfDepartment],
        AcademicAdmin.DeleteSubjectProfessor);

    router.post("/addPracticalMarks", uploadMealsCsv.fields([{name: 'csv', maxCount: 1}]),
        [Middleware.verifyToken, Middleware.isDepartmentEmployee],
        AcademicAdmin.AddPracticalMarks);

    router.get("/exportStudentsList",
        // [Middleware.verifyToken, Middleware.isHeadOfDepartment],
        AcademicAdmin.ExportStudentsList);

    router.post("/sendNotification",
        [Middleware.verifyToken, Middleware.isAffairsSuperAdmin],
        AffAdminController.SendNotification);

    router.get("/filterStudent",
        // [Middleware.verifyToken, Middleware.isAffairsSuperAdmin],
        AffAdminController.FilterStudent);

    router.get("/getDepartments",
        StudentController.GetDepartments);

    router.post("/selectRegisterDate",
        [Middleware.verifyToken, Middleware.isAffairsSuperAdmin],
        AffAdminController.SelectRegisterDate);

    router.put("/updateRegisterDate",
        [Middleware.verifyToken, Middleware.isAffairsSuperAdmin],
        AffAdminController.UpdateRegisterDate);

    router.get("/updateAdmin",
        [Middleware.verifyToken],
        AffAdminController.UpdateAdminActive);

    router.get("/getRegistrationDate",
        [Middleware.verifyToken, Middleware.isAffairsSuperAdmin],
        AffAdminController.GetRegistrationDate);

    router.get("/getExamObjection",
        [Middleware.verifyToken, Middleware.isExaminationAdmin],
        ExaminationAdminController.GetExamObjection);

    router.get("/getObjectionFile",
        [Middleware.verifyToken, Middleware.isExaminationAdmin],
        ExaminationAdminController.GetObjectionFile);

    router.post("/addObjectionFile", uploadMealsCsv.fields([{name: 'csv', maxCount: 1}]),
        [Middleware.verifyToken, Middleware.isExaminationAdmin],
        ExaminationAdminController.AddObjectionFile);

    router.get("/getObjectResultFile",
        [Middleware.verifyToken, Middleware.isExaminationAdmin],
        ExaminationAdminController.GetObjectResultFile);

    router.post("/addSubjectMarks",
        uploadMealsCsv.fields([{name: 'csv', maxCount: 1}]),
        [Middleware.verifyToken, Middleware.isExaminationAdmin],
        AcademicAdmin.AddSubjectMarks);

    router.post("/sendExamNotification",
        [Middleware.verifyToken, Middleware.isExaminationAdmin],
        AcademicAdmin.SendExamNotification);

    router.post("/selectObjectionDate",
        [Middleware.verifyToken, Middleware.isExaminationAdmin],
        ExaminationAdminController.SelectObjectionDate);

    router.put("/updateObjectionDate",
        [Middleware.verifyToken, Middleware.isExaminationAdmin],
        ExaminationAdminController.UpdateObjectionDate);

    router.get("/getObjectionDate",
        [Middleware.verifyToken, Middleware.isExaminationAdmin],
        ExaminationAdminController.GetObjectionDate);

    router.get("/getDepartmentStudentsList",
        [Middleware.verifyToken, Middleware.isDepartmentEmployee],
        ExaminationAdminController.GetDepartmentStudentsList);

    router.get("/getDepartmentProfessors",
        [Middleware.verifyToken, Middleware.isDepartmentEmployee],
        ExaminationAdminController.GetDepartmentProfessors);

    router.get("/getDepartmentProfessors",
        [Middleware.verifyToken, Middleware.isDepartmentEmployee],
        ExaminationAdminController.GetDepartmentProfessors);

    router.get("/getObjectionPracticalFile",
        [Middleware.verifyToken, Middleware.isDepartmentEmployee],
        AcademicAdmin.GetObjectPracticalResultFile);

    router.post("/addObjectionPracticalFile",
        uploadMealsCsv.fields([{name: 'csv', maxCount: 1}]),
        [Middleware.verifyToken, Middleware.isDepartmentEmployee],
        AcademicAdmin.AddSubjectPracticalMarks);

    router.get("/getPracticalObjection",
        [Middleware.verifyToken, Middleware.isDepartmentEmployee],
        AcademicAdmin.GetPracticalObjection);

    router.get("/getExamObjection",
        [Middleware.verifyToken, Middleware.isExaminationAdmin],
        AcademicAdmin.GetExamObjection);

    router.get("/getExamStudentProfile",
        [Middleware.verifyToken, Middleware.isExaminationAdmin],
        AcademicAdmin.GetExamStudentProfile);

    router.post("/insertMessage",
        [Middleware.verifyToken],
        ConversationController.InsertMessage);

    router.get("/getMessages",
        [Middleware.verifyToken],
        ConversationController.GetMessages);

    router.get("/getAdminsActions",
        [Middleware.verifyToken,Middleware.isAffairsSuperAdmin],
        AcademicAdmin.GetAdminsActions);

    router.get("/getAdmins",
        [Middleware.verifyToken],
        AcademicAdmin.GetAdmins);

    router.put("/updateFcm",
        [Middleware.verifyToken],
        AcademicAdmin.UpdateFcm);

    router.post("/yearCreate",
        [Middleware.verifyToken,Middleware.isAffairsSuperAdmin],
        AffAdminController.YearCreate);
    app.use('/admin', router);
}