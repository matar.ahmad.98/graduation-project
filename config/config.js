module.exports = {
  development: {
    username: process.env.USER,
    password: process.env.PASSWORD,
    database: process.env.DB,
    host: process.env.HOST,
    dialect: 'mysql',
    define:{ "charset": "utf8", "dialectOptions": { "collate": "utf8_general_ci" }},
  },
  test: {
    username: process.env.USER,
    password: process.env.PASSWORD,
    database: process.env.DB,
    host: process.env.HOST,
    dialect: 'mysql',
    define:{ "charset": "utf8", "dialectOptions": { "collate": "utf8_general_ci" }},
  },
  production: {
    username: process.env.USER,
    password: process.env.PASSWORD,
    database: process.env.DB,
    host: process.env.HOST,
    dialect: 'mysql',
    define:{ "charset": "utf8", "dialectOptions": { "collate": "utf8_general_ci" }},
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  },
};
// {
//   "development": {
//     "username": "root",
//     "password": null,
//     "database": "testdb1",
//     "host": "127.0.0.1",
//     "dialect": "mysql",
//     "define": { "charset": "utf8", "dialectOptions": { "collate": "utf8_general_ci" }}
//   },
//   "test": {
//     "username": "root",
//     "password": null,
//     "database": "testdb1",
//     "host": "127.0.0.1",
//     "dialect": "mysql"
//
//   },
//   "production": {
//     "username": "root",
//     "password": null,
//     "database": "testdb1",
//     "host": "127.0.0.1",
//     "dialect": "mysql"
//
//   }
// }
