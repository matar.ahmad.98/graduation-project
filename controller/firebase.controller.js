
var admin = require("firebase-admin");

let serviceAccount = require("../config/graduation-application-d4ab0-firebase-adminsdk-s21pd-f24a2024de");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
});

let notification = {
    title: "notification",
    body: 'ddd'
};
let options = {
    priority: "high",
    timeToLive: 60 * 60 * 24
};

exports.send = function (registrationToken,message1){

    const message = {
        data: {
            message:message1
        },
        token: registrationToken
    };
    admin.messaging().send(message)
        .then(function(response) {
            console.log("Successfully sent message:", response);
        })
        .catch(function(error) {
            console.log("Error sending message:", error);
        });
};
