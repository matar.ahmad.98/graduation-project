let db = require("../models");
const {Op} = require("sequelize");
let bcrypt = require("bcryptjs");
let fastcsv = require("fast-csv");
let ExcelJS = require("exceljs");
let fs = require("fs");
const path = require('path');
const baseName = path.basename(__filename);
let Admin = db.Admin;
let Role = db.Role;
let Department = db.Department;
let Professor = db.Professor;
let Semester = db.Semester;
let SSP = db.SemesterSubjectsProfessors;
let SSS = db.SemesterSubjectsStudents;
let Subject = db.Subject;
let Student = db.Student;
let PracticalExam = db.PracticalExam;
let Objection = db.Objection;
let ObjectionNotification = db.ObjectionNotification;
let ObjectionResult = db.ObjectionResult;
let ExamResults = db.ExamResults;
let AcademicYear = db.AcademicYear;
let Firebase = require("../controller/firebase.controller");

exports.GetExamObjection = async (req, res) => {
    let semester = await Semester.findOne({where:{inProgress:true}});
    let expr = {
        where: {statusId: 2,semesterId:semester.id},
        include: [{all: true}, {model:ExamResults,include:Subject}, {model: Student, include: Department, where: {departmentId: req.departmentId}}]
    };
    if (req.roleId === 2)
        expr = {where: {statusId: 2,semesterId:semester.id}, include: [{all: true}, {model:ExamResults,include:Subject}, {model: Student, include: Department}]};
    Objection.findAll(expr)
        .then(obj => {
            res.send({data: obj, status: 200})
        })
        .catch(err => {
            console.log(err)
            res.status(500).send({message: err.message, status: 500})
        })
};

exports.GetObjectionFile = async (req, res) => {
    let semester = await Semester.findOne({where: {inProgress: true}});
    let workbook = new ExcelJS.Workbook(), sheet, model, arr = [], expr = {
        include: [{
            model: ExamResults,
            where: {semesterId: semester.id},
            required: true,
            include: [{model: Student}, {model: Objection, required: true}]
        }],
        where: {departmentId: req.departmentId}
    };
    if (req.roleId === 2)
        expr = {
            include: [{
                model: ExamResults,
                where: {semesterId: semester.id},
                required: true,
                include: [{model: Student}, {model: Objection, required: true}]
            }]
        }

    Subject.findAll(expr)
        .then(subject => {
            for (let i = 0; i < subject.length; i++) {
                arr = [];
                sheet = workbook.addWorksheet(subject[i].name);
                sheet.columns = [
                    {header: "رقم طلب الاعتراض", key: 'objectionId'},
                    {header: "اسم الطالب", key: 'fullName'},
                    {header: "علامة الطالب", key: 'mark'},
                    {header: "علامة الطالب بعد التدقيق", key: 'afterMark'},
                ];
                for (let j = 0; j < subject[i].ExamResults.length; j++) {
                    arr.push({
                        objectionId: subject[i].ExamResults[j].Objection.id,
                        fullName: subject[i].ExamResults[j].Student.firstName + " " + subject[i].ExamResults[j].Student.lastName,
                        mark: subject[i].ExamResults[j].mark
                    })
                }
                sheet.addRows(arr)
            }
            res.setHeader(
                "Content-Type",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            );
            res.setHeader(
                "Content-Disposition",
                "attachment; filename=Students List.xlsx"
            );
            return workbook.xlsx.write(res).then(function () {
                res.status(200).end();
            })
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message,status:500})
        });
}

exports.AddObjectionFile = async (req,res) =>{
    const t = await db.sequelize.transaction();
    let subjectId = req.body.subjectId,data=[],objectiosId = [];
    let semester = await Semester.findOne({where:{inProgress: true}});
    const workbook = new ExcelJS.Workbook();
    await workbook.xlsx.readFile("CsvFile/"+req.files.csv[0].originalname)
        .then( async ()=>{
            workbook.eachSheet((ws,sheetId) => {
                ws.eachRow((row,rowNumber)=>{
                    if (rowNumber !== 1){
                        let objectionId = ws.getCell(`A${rowNumber}`).value;
                        objectiosId.push(objectionId);
                        let originalMark = ws.getCell(`C${rowNumber}`).value;
                        let finalMark = ws.getCell(`D${rowNumber}`).value;
                        let isChange = finalMark!==originalMark;
                        let obj = {objectionId,originalMark,finalMark,isChange,adminId:req.userId};
                        data.push(obj);
                    }
                });
            });
            try {
                await ObjectionResult.bulkCreate(data,{transaction: t});
                await Objection.update({statusId:5},{where:{id:{[Op.in]:objectiosId}},transaction: t});
                data.forEach(dat => {
                    Objection.findByPk(dat.objectionId,{include:[Student]})
                        .then(obj => {
                            if (obj.Student.fcmToken){
                                if (dat.isChange)
                                    Firebase.send(obj.Student.fcmToken,"لقد تمت الاستفادة من الاعتراض واصبحت علامتك الحالية "+dat.finalMark+"علما انها كانت "+dat.originalMark);
                                else Firebase.send(obj.Student.fcmToken,"لم تستفد من الاعتراض وبقيت العلامة "+dat.finalMark)
                            }
                        })
                })
                t.commit();
                res.send({message:"done"})
            }catch (e) {
                t.rollback();
                console.log(e)
                res.status(500).send({message:e.message,status:500})
            }

        });
};

exports.GetObjectResultFile = async (req,res) => {
    let semester =await Semester.findOne({where:{inProgress: true}});
    let workbook = new ExcelJS.Workbook(), sheet, model,arr=[];
    Student.findAll({include:[{model:SSS,where:{subjectId:req.query.subjectId,semesterId:semester.id,isSucceeded: false},include:Student}]})
        .then(sss => {
            for (let i = 0; i <sss.length ; i++) {
                arr.push({id:sss[i].id,fullName:sss[i].firstName +" "+sss[i].lastName})
            }
            sheet = workbook.addWorksheet();
            sheet.columns=[
                {header:"رقم الطالب التعريفي",key:'id'},
                {header:"اسم الطالب",key:'fullName'},
                {header:"علامة الطالب",key:'mark'},
            ];
            // sss.shift();
            console.log(sss);
            sheet.addRows(arr);

            // res is a Stream object
            res.setHeader(
                "Content-Type",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            );
            res.setHeader(
                "Content-Disposition",
                "attachment; filename=Students List.xlsx"
            );

            return workbook.xlsx.write(res).then(function () {
                res.status(200).end();
            })
        });
};

exports.SelectObjectionDate = async (req,res) => {
    let startDate = req.body.startDate, endDate = req.body.endDate, arr = [];
    let semester = await Semester.findOne({where:{inProgress:true}});
    ObjectionNotification.create({startDate,endDate,semesterId:semester.id})
        .then(async ON => {
            res.send({ON,status:200})
            let students = await Student.findAll({where:{fcmToken:{[Op.ne]:null}}});
            console.log(students);
            students.forEach(student => Firebase.send(student.fcmToken,"لقد تم تحديد تاريخ الاعتراض بين "+startDate+" وبين "+endDate));
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message,status:500})
        });
};



exports.UpdateObjectionDate = async (req,res) => {
    let semester = await Semester.findOne({where: {inProgress: true}});
    ObjectionNotification.update({
        startDate: req.body.startDate,
        endDate: req.body.endDate
    }, {where: {semesterId: semester.id}})
        .then(num => {
            Student.findAll()
                .then(std => {
                    std.forEach(s => {
                        if (s.fcmToken)
                            Firebase.send(s.fcmToken, "تم تعديل موعد الاعتراض ليصبح بين " + req.body.startDate + " و " + req.body.endDate);
                    })
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).send({message: err.message, status: 500})
                });
            res.send({message: "Success", status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
}

exports.GetObjectionDate = async (req,res) => {
    let semester = await Semester.findOne({where:{inProgress:true}});
    ObjectionNotification.findOne({where:{semesterId:semester.id}})
        .then(ON => {
            res.send({data:ON,status:200})
        })
        .catch(err => {
            console.log(err)
            res.status(500).send({message: err.message, status: 500})

        })
};

exports.GetDepartmentStudentsList = async (req,res) => {
    let semester =await Semester.findOne({where:{inProgress: true}});
    let workbook = new ExcelJS.Workbook(), sheet;
    Student.findAll({where:{departmentId:req.departmentId}})
        .then(students => {
            for (let i = 1; i <= 5 ; i++) {
                let arr=[];
                sheet = workbook.addWorksheet(`${i}السنة `);
                sheet.columns=[
                    {header:"رقم الطالب التعريفي",key:'id'},
                    {header:"اسم الطالب",key:'fullName'},
                    {header:"حالة الطالب",key:'isFallen'},
                ];
                students.forEach(student => {
                    if (i === student.currentYear)
                        arr.push({id:student.id,fullName:student.firstName+" "+student.lastName,isFallen:student.isFallen?"راسب":"ناجح" })
                });
                sheet.addRows(arr);
            }
            res.setHeader(
                "Content-Type",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            );
            res.setHeader(
                "Content-Disposition",
                "attachment; filename=StudentsList.xlsx"
            );

            return workbook.xlsx.write(res).then(function () {
                res.status(200).end();
            })
        });
};

exports.GetDepartmentProfessors = async (req,res) => {
    let semester = await Semester.findOne({where:{inProgress: true}});
    Professor.findAll({where:{departmentId:req.departmentId},include:[{model:SSP,required:false,where:{semesterId:semester.id},include:[{model:Subject,include:Department}]}]})
        .then(professors => {
            res.send({data:professors,status:200})
        })
        .catch(err => {
            console.log(err)
            res.status(500).send({message: err.message, status: 500})
        })
}