const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
let db = require("../models");
let Admin = db.Admin;
let Role = db.Role;
let bcrypt = require("bcryptjs");

exports.AdminSignIn = (req, res) => {
    console.log("SignIn Function");
    Admin.findOne({
        where: {
            username: req.body.username,
        },
        include: [{model: Role}]
    }).then(async admin => {
        if (!admin) {
            return res.status(404).send({message: "Admin Not found."});
        }
        let passwordIsValid = bcrypt.compareSync(
            req.body.password,
            admin.password
        );
        if (!passwordIsValid) {
            return res.status(401).send({message: "Invalid Password!", status: 401});
        }
        let token = jwt.sign({id: admin.id}, config.secret, {
            expiresIn: 86400 * 720// 2 years
        });
        if (!admin.isActive) {
            return res.status(403).send({message: "Blocked admin", status: 403});
        }
        let Role1 = admin.Role.role;
        res.status(200).send({
            status: 200,
            message: "success",
            data: {
                id: admin.id,
                username: admin.username,
                accessToken: token,
                Role:Role1
            }
        });
    })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500});
        });
};

