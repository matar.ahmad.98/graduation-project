let db = require("../models");
let AReg = db.AReg;
let Student = db.Student;
let Department = db.Department;

exports.findAll = (req,res) => {
    // let limit = parseInt(req.query.limit),page = parseInt(req.query.page);
    let limit = 10,page = 1;
    AReg.findAll({where:{statusId:1},include:[{all:true},{model:Student,include:Department}],limit:limit,offset:(page-1)*limit})
        .then(aregs => {
            res.send(aregs);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message});
        })
};
exports.create = (req,res) => {
    AReg.create({studentId:req.userId,BackID:req.BackID,FrontID:req.FrontID})
        .then(areg => {
            res.send(areg);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message});
        });
};