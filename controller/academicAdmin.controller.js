let db = require("../models");
let Firebase = require("../controller/firebase.controller");
const {Op} = require("sequelize");
let bcrypt = require("bcryptjs");
let fastcsv = require("fast-csv");
let ExcelJS = require("exceljs");
let fs = require("fs");
const path = require('path');
const baseName = path.basename(__filename);
let Admin = db.Admin;
let Role = db.Role;
let Department = db.Department;
let Professor = db.Professor;
let Semester = db.Semester;
let SSP = db.SemesterSubjectsProfessors;
let SSS = db.SemesterSubjectsStudents;
let Subject = db.Subject;
let Student = db.Student;
let PracticalExam = db.PracticalExam;
let ExamResults = db.ExamResults;
let AcademicYear = db.AcademicYear;
let Objection = db.Objection;
let ObjectionResult = db.ObjectionResult;
let PracticalObjection = db.PracticalObjection;
let PracticalObjectionResult = db.PracticalObjectionResult;
let MilitaryDeferment = db.MilitaryDeferment;
let StudentCareer = db.StudentCareer;
let AReg = db.AReg;
let FReg = db.FReg;

exports.getEmployees = (req, res) => {
    let roleId = req.roleId, departmentId = req.departmentId;
    let expr, Role1;
    if (roleId === 1)
        Role1 = [3];
    else if (roleId === 2)
        Role1 = [4];
    else if (roleId === 5)
        Role1 = [6];

    if (departmentId)
        expr = {where: {roleId: {[Op.or]: Role1}, departmentId}, include: [Role, Department]};
    else expr = {where: {roleId: {[Op.or]: Role1}}, include: [Role, Department]};
    Admin.findAll(expr)
        .then(admins => {
            res.send({data: admins, status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
};

exports.CreateEmployee = (req, res) => {
    console.log(req.body)
    let body = {
        username: req.body.username,
        password: bcrypt.hashSync(req.body.password, 8),
        fullName: req.body.fullName,
        roleId: req.body.roleId,
        departmentId: req.departmentId
    };
    if (req.body.type === "exa")
        body = {
            username: req.body.username,
            password: bcrypt.hashSync(req.body.password, 8),
            fullName: req.body.fullName,
            roleId: req.body.roleId,
            departmentId: req.body.departmentId
        }
    Admin.create(body)
        .then(admin => {
            res.send({data: admin, status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
};

exports.GetProfessors = (req, res) => {
    Professor.findAll()
        .then(professors => {
            res.send({data: professors, status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
};

exports.GetSubjects = async (req, res) => {
    let sem = await Semester.findOne({where: {[Op.and]: [{inProgress: true}]}});
    SSP.findAll({
        where: {[Op.and]: [{semesterId: sem.id}, {isActive: 1}]},
        include: [{model: Professor, include: Department}, {model: Subject, where: {departmentId: req.departmentId}}]
    })
        .then(sub => {
            res.send({data: sub, status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
};

exports.LinkSubjectWithProfessor = async (req, res) => {
    let sem = await Semester.findOne({where: {inProgress: true}});

    SSP.create({semesterId: sem.id, professorId: req.body.professorId, subjectId: req.body.subjectId})
        .then(ssp => {
            res.send({data: ssp, status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
};

exports.UpdateLinkSubjectWithProfessor = async (req, res) => {
    console.log(req.body);
    SSP.update(req.body, {where: {id: req.body.sspId}})
        .then(ssp => {
            res.send({data: ssp, status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
}

exports.GetAllSubjects = async (req, res) => {
    let sem = await Semester.findOne({where: {inProgress: true}});

    Subject.findAll({
        where: {departmentId: req.departmentId},
        include: [{model: Professor, attributes: ["id"], through: {where: {semesterId: sem.id, isActive: true}}}]
    })
        .then(sub => {
            sub.forEach((s1, i1) => {
                let professorsId = [];
                s1.dataValues.Professors.forEach((s2, i2) => {
                    professorsId.push(s2.id);
                });
                sub[i1].dataValues.professorsId = professorsId;
            });
            res.send({data: sub, status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
};

exports.DeleteSubjectProfessor = (req, res) => {
    SSP.update({isActive: 0}, {where: {id: req.body.sspId}})
        .then(num => {
            res.send({message: "Done", status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
};

exports.AddPracticalMarks = async (req, res) => {
    let subjectId = req.body.subjectId, data = [];
    let semester = await Semester.findOne({where: {inProgress: true}});
    const workbook = new ExcelJS.Workbook();
    await workbook.xlsx.readFile("CsvFile/" + req.files.csv[0].originalname)
        .then(() => {
            let ws = workbook.getWorksheet("sheet1");
            ws.eachRow((row, rowNumber) => {
                let studentId = ws.getCell(`A${rowNumber}`).value;
                let mark = ws.getCell(`C${rowNumber}`).value;
                Student.findByPk(studentId)
                    .then(st => {
                        Subject.findByPk(subjectId)
                            .then(sub => {
                                if (st.fcmToken)
                                    Firebase.send(st.fcmToken,"نلت علامة "+mark+"بفحص العملي بمادة"+sub.name)
                            });
                    });
                let obj = {
                    studentId: studentId,
                    mark: mark,
                    subjectId: subjectId,
                    semesterId: semester.id,
                    adminId: req.userId
                };
                data.push(obj);
            });
            data.shift();
            PracticalExam.bulkCreate(data)
                .then(meals => {
                    res.send({meals, status: 200, message: "success"})
                })
                .catch(err => {
                    res.status(500).send({message: "cant create bulk meals because " + err.message})
                });
        });

    // let stream = fs.createReadStream("CsvFile/"+req.files.csv[0].originalname);
    // let csvData = [],practicalExams=[];
    // let csvStream = fastcsv
    //     .parse()
    //     .on("data", function(data) {
    //         csvData.push(data);
    //     })
    //     .on("end", function() {
    //         // remove the first line: header
    //         csvData.shift();
    //         console.log(csvData);
    //         for (let i = 0; i < csvData.length; i++) {
    //             let obj={studentId:csvData[i][0],name:csvData[i][1],mark:csvData[i][2],subjectId:req.body.subjectId,semesterId:semester.id,adminId:req.userId};
    //             practicalExams.push(obj)
    //         }
    //         PracticalExam.bulkCreate(practicalExams)
    //             .then(meals => {
    //                 res.send({meals,status:200,message:"success"})
    //             })
    //             .catch(err => {
    //                 res.status(500).send({message:"cant create bulk meals because "+err.message})
    //             });
    //         // connect to the MySQL database
    //         // save csvData
    //     });
    // stream.pipe(csvStream);

}

exports.ExportStudentsList = async (req, res) => {
    let semester = await Semester.findOne({where: {inProgress: true}});
    console.log(semester.id)
    let workbook = new ExcelJS.Workbook(), sheet, model, arr = [];
    Student.findAll({
        include: [{
            model: SSS,
            where: {subjectId: req.query.subjectId, semesterId: semester.id, isSucceeded: false},
            include: Student
        }]
    })
        .then(sss => {
            for (let i = 0; i < sss.length; i++) {
                arr.push({id: sss[i].id, fullName: sss[i].firstName + " " + sss[i].lastName})
            }
            sheet = workbook.addWorksheet();
            sheet.columns = [
                {header: "رقم الطالب التعريفي", key: 'id'},
                {header: "اسم الطالب", key: 'fullName'},
                {header: "علامة الطالب", key: 'mark'},
            ];
            // sss.shift();
            console.log(sss);
            sheet.addRows(arr);

            // res is a Stream object
            res.setHeader(
                "Content-Type",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            );
            res.setHeader(
                "Content-Disposition",
                "attachment; filename=Students List.xlsx"
            );

            return workbook.xlsx.write(res).then(function () {
                res.status(200).end();
            })
        });
}

exports.AddSubjectMarks = async (req, res) => {
    let promises = [];
    let semester = await Semester.findOne({where: {inProgress: true}, include: AcademicYear});
    let subjectId = req.body.subjectId, data = [], arr = [];
    let subject = await Subject.findByPk(subjectId);
    const workbook = new ExcelJS.Workbook();
    await workbook.xlsx.readFile("CsvFile/" + req.files.csv[0].originalname)
        .then(async () => {
            let ws = workbook.getWorksheet("sheet1");
            ws.eachRow((row, rowNumber) => {
                if (rowNumber !== 1) {
                    let studentId = ws.getCell(`A${rowNumber}`).value;
                    let mark = ws.getCell(`C${rowNumber}`).value;
                    data.push({mark, studentId});
                }
            });
            for (let i = 0; i < data.length; i++) {
                let studentId = data[i].studentId, mark = data[i].mark;
                let practicalMark = await PracticalExam.findOne({where: {studentId, subjectId}});
                let student = await Student.findByPk(studentId);
                let totalMark = practicalMark.mark + mark;
                if (totalMark >= 60) {
                    let sss = SSS.update({isSucceeded: true}, {where: {studentId, semesterId: semester.id, subjectId}});
                    promises.push(sss);
                    if (student.fcmToken) {
                        Firebase.send(student.fcmToken, `لقد نجحت في مادة ${subject.name} بمعدل كلي ${totalMark}% علما ان علامة العملي ${practicalMark.mark} والنظري ${mark}`);
                    }
                } else {
                    if (student.fcmToken) {
                        Firebase.send(student.fcmToken, `لقد رسبت في مادة ${subject.name} بمعدل كلي ${totalMark}% علما ان علامة العملي ${practicalMark.mark} والنظري ${mark}`);
                    }
                }
                let obj = {
                    studentId,
                    mark,
                    practicalMarkId: practicalMark.id,
                    subjectId: subjectId,
                    semesterId: semester.id,
                    adminId: req.userId
                };
                arr.push(obj);
            }
            ExamResults.bulkCreate(arr)
                .then(examResults => {
                    res.send({examResults, status: 200, message: "success"});
                    isYearFinished(semester, req.departmentId);
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).send({message: "cant create bulk meals because " + err.message})
                });
        });

}

let isYearFinished = async (semester, departmentId) => {
    let ER = await ExamResults.findAll({
        where: {},
        include: [{
            model: Semester,
            required: true,
            include: [{model: AcademicYear, where: {year: semester.AcademicYear.year}}]
        }, {model: Subject, where: {departmentId}}],
        attributes: ['subjectId'],
        group: ['subjectId']
    })
    ER.map(er => er.subjectId);
    let de = await Department.findByPk(1);
    console.log({"عدد المواد المدخلة": ER.length, "عدد المواد الكلية للقسم": de.subjectsCount})
    if (de.subjectsCount === ER.length) {
        Student.findAll({where: {departmentId: departmentId}, include: [{model: SSS}]})
            .then(students => {
                students.forEach(student => {
                    let fallenSubjects = 0
                    student.SemesterSubjectsStudents.forEach(sss => {
                        if (sss.isSucceeded === 0)
                            fallenSubjects++;
                    });
                    if (fallenSubjects < 4) {
                        Student.update({
                            currentYear: student.currentYear + 1,
                            isFallen: false
                        }, {where: {id: student.id}})
                        if (student.fcmToken)
                            Firebase.send(student.fcmToken, ` تهانينا لقد نجحت بالسنة الحالية علما انه قد رسبت بـ ${fallenSubjects} مادة `)

                        console.log(`${student.username} has fallen in ${fallenSubjects} subjects and he will repeat the year...bloody bastard!`);
                    } else {
                        Student.update({isFallen: true}, {where: {id: student.id}});
                        if (student.fcmToken)
                            Firebase.send(student.fcmToken, ` للاسف لقد رسبت بالسنة الحالية علما انه قد رسبت بـ ${fallenSubjects} مادة `)
                        console.log(`${student.username} has fallen in ${fallenSubjects} subjects and he succeeded to the next academic year`);
                    }
                })
            })
            .catch(err => {
                console.log(err)
            })
    }

}

exports.SendExamNotification = (req, res) => {
    let studentId = req.body.studentId, text = req.body.text;

    Student.findByPk(studentId)
        .then(student => {
            if (student.fcmToken)
                Firebase.send(student.fcmToken, text);
            res.send({message: "done", status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
};

exports.GetObjectPracticalResultFile = async (req, res) => {
    let semester = await Semester.findOne({where: {inProgress: true}});
    let workbook = new ExcelJS.Workbook(), sheet, model, arr = [],
        expr = {
            include: [{
                model: PracticalExam,
                where: {semesterId: semester.id},
                required: true,
                include: [{model: Student}, {model: PracticalObjection, required: true}]
            }],
            where: {departmentId: req.departmentId}
        };

    Subject.findAll(expr)
        .then(subject => {
            for (let i = 0; i < subject.length; i++) {
                arr = [];
                sheet = workbook.addWorksheet(subject[i].name);
                sheet.columns = [
                    {header: "رقم طلب الاعتراض", key: 'objectionId'},
                    {header: "اسم الطالب", key: 'fullName'},
                    {header: "علامة الطالب", key: 'mark'},
                    {header: "علامة الطالب بعد التدقيق", key: 'afterMark'},
                ];
                for (let j = 0; j < subject[i].PracticalExams.length; j++) {
                    arr.push({
                        objectionId: subject[i].PracticalExams[j].PracticalObjection.id,
                        fullName: subject[i].PracticalExams[j].Student.firstName + " " + subject[i].PracticalExams[j].Student.lastName,
                        mark: subject[i].PracticalExams[j].mark
                    })
                }
                sheet.addRows(arr)
            }
            res.setHeader(
                "Content-Type",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            );
            res.setHeader(
                "Content-Disposition",
                "attachment; filename=Students List.xlsx"
            );
            return workbook.xlsx.write(res).then(function () {
                res.status(200).end();
            })
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        });
};

exports.AddSubjectPracticalMarks = async (req, res) => {
    const t = await db.sequelize.transaction();
    let subjectId = req.body.subjectId, data = [], objectiosId = [];
    // let semester = await Semester.findOne({where:{inProgress: true}});
    const workbook = new ExcelJS.Workbook();
    await workbook.xlsx.readFile("CsvFile/" + req.files.csv[0].originalname)
        .then(async () => {
            workbook.eachSheet((ws, sheetId) => {
                ws.eachRow((row, rowNumber) => {
                    if (rowNumber !== 1) {
                        let practicalObjectionId = ws.getCell(`A${rowNumber}`).value;
                        objectiosId.push(practicalObjectionId);
                        let originalMark = ws.getCell(`C${rowNumber}`).value;
                        let finalMark = ws.getCell(`D${rowNumber}`).value;
                        let isChange = finalMark !== originalMark;
                        if (isChange)
                            PracticalObjection.findByPk(practicalObjectionId, {include: PracticalExam})
                                .then(PO => {
                                    PracticalExam.update({mark: finalMark}, {where: {id: PO.PracticalExam.id}})
                                        .catch(err => {
                                            console.log(err);
                                            res.status(500).send({message: err.message, status: 500})
                                        })
                                })
                        let obj = {practicalObjectionId, originalMark, finalMark, isChange, adminId: req.userId};
                        data.push(obj);
                    }
                });
            });
            try {
                await PracticalObjectionResult.bulkCreate(data, {transaction: t});
                await PracticalObjection.update({statusId: 2}, {where: {id: {[Op.in]: objectiosId}}, transaction: t});
                data.forEach(dat => {
                    PracticalObjection.findByPk(dat.practicalObjectionId, {include: [Student]})
                        .then(obj => {
                            if (obj.Student.fcmToken) {
                                if (dat.isChange)
                                    Firebase.send(obj.Student.fcmToken, "لقد واصبحت علامتك الحالية بعد الاعتراض " + dat.finalMark + "علما انها كانت " + dat.originalMark);
                                else Firebase.send(obj.Student.fcmToken, "لم تستفد من الاعتراض وبقيت العلامة " + dat.finalMark)
                            }
                        })
                })
                t.commit();
                res.send({message: "done"})
            } catch (e) {
                t.rollback();
                console.log(e)
                res.status(500).send({message: e.message, status: 500})
            }

        });
}

exports.GetPracticalObjection = async (req, res) => {
    let semester = await Semester.findOne({where: {inProgress: true}});
    PracticalObjection.findAll({
        where: {semesterId: semester.id},
        include: [{model: PracticalObjectionResult}, {model: Student}, {model: PracticalExam, include: Subject}]
    })
        .then(PE => {
            res.send({data: PE, status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })

}

exports.GetExamObjection = async (req, res) => {
    let semester = await Semester.findOne({where: {inProgress: true}});
    Objection.findAll({where: {semesterId: semester.id}, include: ObjectionResult})
        .then(PE => {
            res.send({data: PE, status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })

}

exports.GetExamStudentProfile = async (req, res) => {
    let studentId = req.query.studentId;
    try {
        let student = await Student.findByPk(studentId);
        let AY = await AcademicYear.findAll({
            attributes: ["year"],
            include: [{
                model: Semester,
                attributes: ["semesterNumber"],
                required: true,
                include: [{
                    attributes: ["mark"],
                    model: ExamResults,
                    where: {studentId},
                    include: [{model: Subject, attributes: ["name", "yearNumber"]}, {
                        model: PracticalExam,
                        attributes: ["mark"]
                    }],
                    required: true
                }]
            }]
        })
        res.send({data: {page1: student, page2: AY}, status: 200})
    } catch (e) {
        console.log(e);
        res.status(500).send({message: e.message, status: 500})
    }
};

exports.GetAdminsActions = async (req, res) => {
    let arr = [],adminsId = [], admins, statement;
    statement = {where: {adminId: {[Op.in]: adminsId}},include:[Student,Admin]};
    admins = await Admin.findAll({where: {roleId: 3}});
    admins.forEach(admin => {
        adminsId.push(admin.id)
    })
    console.log(adminsId);
    MilitaryDeferment.findAll(statement)
        .then(MDs => {
            for (let i = 0; i < MDs.length; i++) {
                if (MDs[i].statusId === 2){
                    arr.push({name:MDs[i].Admin.username,action:"الموافقة على طلب مصدقة التأجيل للطالب "+MDs[i].Student.username})
                }
                if (MDs[i].statusId === 3){
                    arr.push({name:MDs[i].Admin.username,action:"رفض طلب مصدقة التأجيل للطالب "+MDs[i].Student.username})
                }
            }
            StudentCareer.findAll(statement)
                .then(SCs => {
                    for (let i = 0; i < SCs.length; i++) {
                        if (SCs[i].statusId === 2){
                            arr.push({name:SCs[i].Admin.username,action:"الموافقة على طلب الحياة الجامعية للطالب "+SCs[i].Student.username})
                        }
                        if (SCs[i].statusId === 3){
                            arr.push({name:SCs[i].Admin.username,action:"رفض طلب الحياة الجامعية للطالب "+SCs[i].Student.username})
                        }
                    }
                    AReg.findAll(statement)
                        .then(ARs => {
                            for (let i = 0; i < ARs.length; i++) {
                                if (ARs[i].statusId === 2){
                                    arr.push({name:ARs[i].Admin.username,action:"الموافقة على طلب التسجيل السنوي للطالب "+ARs[i].Student.username})
                                }
                                if (ARs[i].statusId === 3){
                                    arr.push({name:ARs[i].Admin.username,action:"رفض طلب التسجيل السنوي للطالب "+ARs[i].Student.username})
                                }
                            }
                            Objection.findAll(statement)
                                .then(Os => {
                                    console.log(Os);
                                    for (let i = 0; i < Os.length; i++) {
                                        if (Os[i].statusId === 2){
                                            arr.push({name:Os[i].Admin.username,action:"الموافقة على طلب الاعتراض للطالب "+Os[i].Student.username})
                                        }
                                        if (Os[i].statusId === 3){
                                            arr.push({name:Os[i].Admin.username,action:"رفض طلب الاعتراض للطالب "+Os[i].Student.username})
                                        }
                                    }
                                    FReg.findAll(statement)
                                        .then(FRs => {
                                            console.log(FRs);
                                            for (let i = 0; i < FRs.length; i++) {
                                                if (FRs[i].statusId === 2){
                                                    arr.push({name:FRs[i].Admin.username,action:"الموافقة على طلب التسجيل في الكلية للطالب "+FRs[i].Student.username})
                                                }
                                                if (FRs[i].statusId === 3){
                                                    arr.push({name:FRs[i].Admin.username,action:"رفض طلب التسجيل في الكلية"})
                                                }
                                            }
                                            res.send({data:arr})
                                        })
                                        .catch(err => {
                                            console.log(err);
                                            res.status(500).send({message:err.message,status:500})
                                        });
                                })
                                .catch(err => {
                                    console.log(err);
                                    res.status(500).send({message:err.message,status:500})
                                });
                        })
                        .catch(err => {
                            console.log(err);
                            res.status(500).send({message:err.message,status:500})
                        });
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).send({message:err.message,status:500})
                });
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message,status:500})
        });
};

exports.GetAdmins = async (req,res) => {
    let Ad = [];
    if (req.roleId===1){
        Ad = await Admin.findAll({where:{roleId:3}})
    }
    if (req.roleId===2){
        Ad = await Admin.findAll({where:{roleId:4}})
    }
    if (req.roleId===5){
        Ad = await Admin.findAll({where:{roleId:6,departmentId:req.departmentId}})
    }
    res.send(Ad);
};

exports.UpdateFcm = (req,res) => {
    Admin.update({fcmToken:req.body.fcmToken},{where:{adminId:req.userId}})
        .then(num => {
            res.send({message:"Done"})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message})
        })
}