let db = require("../models");
let StudentCareer = db.StudentCareer;
let Student = db.Student;
let Department = db.Department;

exports.create = (req,res) => {
    StudentCareer.create({studentId:req.userId,image:req.fName})
        .then(studentCareer => {
            res.send(studentCareer);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message});
        });
}

exports.findAll = (req,res) => {
    StudentCareer.findAll({where:{statusId:1},include:[{all:true},{model:Student,include:Department}]})
        .then(StudentCareers => {
            res.send(StudentCareers);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message});
        });
}
