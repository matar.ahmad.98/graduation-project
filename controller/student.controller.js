let bcrypt = require("bcryptjs");
let {Op} = require("sequelize");
const config = require("../config/auth.config");
let jwt = require("jsonwebtoken");
let db = require("../models");
let PendingStudent = db.PendingStudent;
let Student = db.Student;
let Department = db.Department;
let MilitaryDepartment = db.MilitaryDepartment;
let ForeignLanguage = db.ForeignLanguage;
let ExamResults = db.ExamResults;
let Semester = db.Semester;
let PracticalExam = db.PracticalExam;
let AcademicYear = db.AcademicYear;
let Subject = db.Subject;


exports.PostPendingStudent = (req, res) => {
    PendingStudent.create(req.body)
        .then(PeSt => {

        })
        .catch(err => {

        })
};

exports.GetDepartments = (req, res) => {
    Department.findAll()
        .then(deps => {
            res.send({data: deps, status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
};

exports.GetDepartments = (req, res) => {
    Department.findAll()
        .then(deps => {
            res.send({data: deps, status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
};

exports.GetMilitaryDepartments = (req, res) => {
    MilitaryDepartment.findAll()
        .then(MD => {
            res.send({data: MD, status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
};

exports.GetForeignLanguages = (req, res) => {
    ForeignLanguage.findAll()
        .then(FL => {
            res.send({data: FL, status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
};

exports.SignIn = (req, res) => {
    Student.findOne({where: {username: req.body.username}})
        .then(student => {
            if (!student) {
                PendingStudent.findOne({where: {username: req.body.username}})
                    .then(pendingStudent => {
                        if (!pendingStudent)
                            return res.status(400).send({message: "No User for this username"})
                        else {
                            let passwordIsValid = bcrypt.compareSync(
                                req.body.password,
                                pendingStudent.password
                            );
                            if (!passwordIsValid) {
                                res.status(401).send({message: "Invalid Password!"})
                            } else {
                                let accessToken = jwt.sign({id: pendingStudent.id, studentType: "Pending"}, config.secret, {
                                    expiresIn: 8640000 // 24 hours
                                });
                                res.send({data: pendingStudent, accessToken,type:"Pending"})
                            }
                        }
                    })
                    .catch(err => {
                        console.log(err);
                        res.status(500).send({message:err.message,status:500})
                    })
            } else {
                let passwordIsValid = bcrypt.compareSync(
                    req.body.password,
                    student.password
                );
                if (!passwordIsValid) {
                    res.status(401).send({message: "Invalid Password!"})
                } else {
                    let accessToken = jwt.sign({id: student.id, studentType: "Approved"}, config.secret, {
                        expiresIn: 86400 // 24 hours
                    });
                    res.send({data: student, accessToken,type:"Approved"})
                }
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message,status:500})
        })
};

exports.UpdateFcmToken = (req,res) => {
    Student.update({fcmToken:req.body.fcmToken},{where:{id:req.userId}})
        .then(num => {
            res.send({message: "Success"})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message,status:500})
        })
};


exports.GetStudentResults = (req,res) => {
    let semester = Semester.findByPk(req.body.semesterId);
    ExamResults.findAll({where:{[Op.and]:[{semesterId:semester.id},{studentId:req.userId}]}})
        .then(ER => {
            PracticalExam.findAll({where:{[Op.and]:[{semesterId:semester.id},{studentId:req.userId}]}})
                .then(PE=>{
                    res.send({data:{examResult:ER,practicalExam:PE},status:200})
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).send({message:err.message,status:500})
                })
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message,status:500})
        })
};

exports.GetStudentProfile = (req,res) => {
    Student.findByPk(req.userId)
        .then(student => {
            res.send({data:student,status:200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message,status:500})
        });
};

exports.UpdateStudentProfile = (req,res) => {
    // let obj = {username:}
}

exports.GetMarks = (req,res) => {
    ExamResults.findAll({where:{[Op.and]:[{semesterId:req.body.semesterId},{studentId:req.userId}]},include:[{model:PracticalExam},{model:Subject}]})
        .then(ERs => {
            res.send({data:ERs,message:"Success"})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message,status:500})
        });
}

exports.GetMarksStuff = (req,res) => {
    AcademicYear.findAll()
        .then(AYs => {
            Semester.findAll()
                .then(Ss => {
                res.send({semesters:Ss,AcademicYears:AYs,message:"Success"})
            })
                .catch(err => {
                    console.log(err);
                    res.status(500).send({message:err.message,status:500})
                })
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message,status:500})
        })
}