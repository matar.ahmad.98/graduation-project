let db = require("../models");
const {Op} = require("sequelize");
let Admin = db.Admin;
let PendingStudent = db.PendingStudent;
let Student = db.Student;
let MilitaryDepartment = db.MilitaryDepartment;
let ForeignLanguage = db.ForeignLanguage;
let Department = db.Department;
let Notification = db.Notification;
let Firebase = require("../controller/firebase.controller");
let RegisterNotification = db.RegisterNotification;
let Semester = db.Semester;
let Subject = db.Subject;
let SemesterSubjectsStudents = db.SemesterSubjectsStudents;
let FReg = db.FReg;
let AcademicYear = db.AcademicYear;



exports.getPendingStudents = (req, res) => {
    PendingStudent.findAll({include: [ForeignLanguage, Department, MilitaryDepartment]})
        .then(PeSt => {
            res.send(PeSt);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
};

exports.ApprovePendingStudent = (req, res) => {
    PendingStudent.findByPk(req.body.studentId)
        .then(APS => {
            if (!APS)
                return res.status(401).send({message: "no user with this Id"});
            let {id,...st} = APS.dataValues;
            Student.create(st)
                .then(student => {
                    res.send(student);
                    Subject.findAll({where:{departmentId:APS.departmentId,yearNumber:1}})
                        .then(async subjects => {
                            let semester = await Semester.findOne({where:{inProgress:true}}), arr = [];
                            subjects.forEach(sub => {
                                arr.push({studentId:student.id,semesterId:semester.id,subjectId:sub.id})
                            });
                            console.log(arr);
                            SemesterSubjectsStudents.bulkCreate(arr).catch(err=> {
                                console.log(err);
                                res.status(500).send({message:err.message,status:500})
                            });
                        });
                    PendingStudent.destroy({where: {id: req.body.studentId}});
                    //Notification to User that he is Approved
                    FReg.create({adminId:req.userId,studentId:student.id,statusId:2})
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).send({message: err.message, status: 500})
                })
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
};

exports.RejectPendingStudent = (req, res) => {
    PendingStudent.destroy({where: {id: req.body.studentId}})
        .then(PS => {
            res.send({data: PS, message: "success"})
            FReg.create({adminId:req.userId,statusId:3})
            //Notification to User that rejected
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
};

exports.SendNotification = (req, res) => {
    let expr, departmentId = req.body.departmentId, type = req.body.type;
    if (type === "PA") {
        expr = {where: {id: req.body.studentId}}
    } else {
        if (departmentId === -1)
            expr = {};
        else
            expr = {where: {departmentId}};
    }
    Notification.create({text: req.body.text, adminId: req.userId})
        .then(notif => {
            //Send Notification to Students
            Student.findAll(expr)
                .then(std => {
                    std.forEach(st => {
                        if (st.fcmToken) {
                            Firebase.send(st.fcmToken, req.body.text);
                            console.log(st.fcmToken);

                        }
                    });
                    res.send({data: notif, status: 200})
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).send({message: err.message, status: 500})
                })
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
}

exports.FilterStudent = (req, res) => {
    let expr = {
        distinct: true,
        where: {
            [Op.or]: [
                {firstName: {[Op.substring]: req.query.name}},
                {lastName: {[Op.substring]: req.query.name}},
                {username: {[Op.substring]: req.query.name}}
            ]
        }
    };
    if (req.query.departmentId)
        expr = {
            distinct: true,
            where: {
                [Op.or]: [
                    {firstName: {[Op.substring]: req.query.name}},
                    {lastName: {[Op.substring]: req.query.name}},
                    {username: {[Op.substring]: req.query.name}}
                ],
                departmentId: req.query.departmentId
            }
        }
    Student.findAll(expr)
        .then(std => {
            let arr = [];
            std.forEach(s => {
                let fullName = s.dataValues.firstName + " " + s.dataValues.lastName;
                arr.push({fullName, id: s.id})
            })
            res.send({data: arr})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
}


exports.SelectRegisterDate = async (req, res) => {
    console.log(req.body.firstDate)

    let semester = await Semester.findOne({where: {inProgress: true}});
    RegisterNotification.create({startDate: req.body.firstDate, endDate: req.body.lastDate, semesterId: semester.id})
        .then(RN => {
            Student.findAll()
                .then(std => {
                    std.forEach(s => {
                        if (s.fcmToken)
                            Firebase.send(s.fcmToken, "تم تحديد موعد التسجيل بين " + req.body.firstDate + " و " + req.body.lastDate);
                    })
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).send({message: err.message, status: 500})
                });
            res.send({data: RN, status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
}
exports.UpdateRegisterDate = async (req, res) => {
    let semester = await Semester.findOne({where: {inProgress: true}});
    RegisterNotification.update({
        startDate: req.body.firstDate,
        endDate: req.body.lastDate
    }, {where: {semesterId: semester.id}})
        .then(num => {
            Student.findAll()
                .then(std => {
                    std.forEach(s => {
                        if (s.fcmToken)
                            Firebase.send(s.fcmToken, "تم تعديل موعد التسجيل ليصبح بين " + req.body.firstName + " و " + req.body.lastDate);
                    })
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).send({message: err.message, status: 500})
                });
            res.send({message: "Success", status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
}

exports.UpdateAdminActive = (req, res) => {
    let adminId = req.query.adminId;
    Admin.findByPk(adminId)
        .then(admin => {
            let status = admin.isActive;
            Admin.update({isActive: !status}, {where:{id: adminId}})
                .then(num => {
                    res.send({message: "Success", status: 200})
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).send({message: err.message, status: 500})
                })
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
};

exports.GetRegistrationDate = async (req, res) => {
    let semester = await Semester.findOne({where: {inProgress: true}})
    RegisterNotification.findAll({where: {semesterId: semester.id}})
        .then(RN => {
            res.send({data: RN, status: 200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message: err.message, status: 500})
        })
}

exports.YearCreate =async (req,res) => {
    let semester = await Semester.findOne({where:{inProgress:true},include:AcademicYear});
    if (semester.semesterNumber === 2){
        Semester.update({inProgress:false},{where:{id:semester.id}})
            .then(sem => {
                let newYear = parseInt(semester.AcademicYear.year)+1;
                AcademicYear.create({year:newYear})
                    .then(AY => {
                        Semester.create({yearId:AY.id,semesterNumber:1,inProgress:1})
                            .catch(err => {
                                console.log(err);
                                res.status(500).send({message: err.message, status: 500})
                            });
                        Semester.create({yearId:AY.id,semesterNumber:2,inProgress:0})
                            .catch(err => {
                                console.log(err);
                                res.status(500).send({message: err.message, status: 500})
                            });
                        res.send({message:"The Academic year has begin"})
                    })
                    .catch(err => {
                        console.log(err);
                        res.status(500).send({message: err.message, status: 500})
                    })
            })
            .catch(err => {
                console.log(err);
                res.status(500).send({message: err.message, status: 500})
            })
    }
    else {
        await Semester.update({inProgress:false},{where:{id:semester.id}});
        Semester.findOne({where:{[Op.and]:[{yearId:semester.yearId},{id:{[Op.ne]:semester.id}}]}})
            .then(s => {
                Semester.update({inProgress:true},{where:{[Op.and]:[{id:s.id}]}})
                    .then(num => {
                        res.send({message:"The next Semester has begin"})
                    })
                    .catch(err => {
                        console.log(err);
                        res.status(500).send({message: err.message, status: 500})
                    })
            })
            .catch(err => {
                console.log(err);
                res.status(500).send({message: err.message, status: 500})
            })
    }
};