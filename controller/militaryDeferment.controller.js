let db = require("../models");
let MilitaryDeferment = db.MilitaryDeferment;
let Student = db.Student;
let Department = db.Department;
let Firebase = require("../controller/firebase.controller");

exports.create = (req,res) => {
    MilitaryDeferment.create({studentId:req.userId})
        .then(militaryDeferment => {
            res.send(militaryDeferment);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message});
        });
}

exports.findAll = (req,res) => {
    MilitaryDeferment.findAll({where:{statusId:1},include:[{all:true},{model:Student,include:Department}]})
        .then(militaryDeferments => {
            res.send(militaryDeferments);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message});
        });
}

exports.UpdateRequestStatus = async (req,res) => {
    const t = await db.sequelize.transaction();

    if (req.body.requestType !=="AReg" && req.body.requestType !=="StudentCareer" && req.body.requestType !=="Objection" && req.body.requestType !=="MilitaryDeferment")
        return res.status(401).send({message:"wrong requestType: "+req.body.requestType});

    db[`${req.body.requestType}`].update({statusId:req.body.statusId,adminId:req.userId},{where:{id:req.body.requestId},transaction: t})
        .then(async RT => {
            if (req.body.requestType === "AReg" && req.body.statusId === 2){
                let AReg = await db.AReg.findByPk(req.body.requestId);
                let student = await Student.findByPk(AReg.studentId);
                Student.update({currentYear:student.currentYear+1},{where:{id:AReg.studentId},transaction: t})
                    .then(s => {
                        Student.findByPk(AReg.studentId)
                            .then(s => {
                                Firebase.send(s.fcmToken,"تم قبول طلبك")
                            })
                        t.commit();
                        return res.send({message:"Success",status:200})
                    })
                    .catch(err => {
                        console.log(err);
                        t.rollback();
                        return res.status(500).send({message:err.message,status:500})
                    })
            }
            else if (req.body.statusId===3){
                Student.findByPk(req.body.studentId)
                    .then(s => {
                        Firebase.send(s.fcmToken,"تم رفض طلبك")
                    })
                res.send({message:"Success",status:200})
                t.commit();

            }
            else{
                console.log(req.body.studentId);
                Student.findByPk(req.body.studentId)
                    .then(s => {
                        Firebase.send(s.fcmToken,"تم قبول طلبك")
                    })
                t.commit();
                //Send Notification to Student to notify him with the new state of his request
                res.send({message:"Success",status:200})
            }
        })
        .catch(err => {
            console.log(err);
            t.rollback();
            res.status(500).send({message:err.message,status:500});
        })
}