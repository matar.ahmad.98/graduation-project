let bcrypt = require("bcryptjs");
let db = require("../models");
let jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
let FReg = db.FReg;
let PendingStudent = db.PendingStudent;
let Student = db.Student;
let Department = db.Department;

exports.findAll = (req,res) => {
    FReg.findAll({include:[{all:true},{model:Student,include:Department}]})
        .then(fregs => {
            res.send(fregs);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message});
        })
};
exports.create = (req,res) => {
    console.log(req.body);
    PendingStudent.create({
        firstName:req.body.firstName,
        lastName:req.body.lastName,
        username:req.body.username,
        email:req.body.email,
        phoneNumber:req.body.phoneNumber,
        password:bcrypt.hashSync(req.body.password, 8),
        departmentId:req.body.departmentId,
        militaryDepartmentId:req.body.militaryDepartmentId,
        foreignLanguageId:req.body.foreignLanguageId,
        housingNumber:req.body.housingNumber,
        PersonalPic:req.PersonalPic,
        Certificate:req.Certificate,
        FrontID:req.FrontID,
        BackID:req.BackID
    })
        .then(PS => {
            let token = jwt.sign({accountType:"pending",id:PS.id}, config.secret, {
                expiresIn: 8640000 // 24 hours
            });
            res.send({data:PS,token,status:200});
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message});
        });
};