let db = require("../models");
let Admin = db.Admin;
let Role = db.Role;
let bcrypt = require("bcryptjs");

exports.AdminAffSignUp = (req,res) => {
    console.log("Affairs Admin SignUp");
    Admin.create({username:req.body.username,password:bcrypt.hashSync(req.body.password, 8),fullName:req.body.fullName,roleId:3})
        .then(admin => {
            res.send(admin);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message,status:500});
        });
};
exports.AdminExaSignUp = (req,res) => {
    console.log("Examination Admin SignUp");
    Admin.create({username:req.body.username,password:bcrypt.hashSync(req.body.password, 8),fullName:req.body.fullName,roleId:4})
        .then(admin => {
            res.send(admin);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message,status:500});
        });
};

exports.findAllAffairAdmins = (req,res) => {
    console.log("Admin findAll");
    Admin.findAll({where:{roleId:3},include:Role})
        .then(admin => {
            res.send(admin);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message,status:500});
        });
};
exports.findAllExaminationAdmins = (req,res) => {
    console.log("Admin findAll");
    Admin.findAll({where:{roleId:4},include:Role})
        .then(admin => {
            res.send(admin);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message,status:500});
        });
};