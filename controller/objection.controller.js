let db = require("../models");
let Objection = db.Objection;
let Student = db.Student;
let Department = db.Department;
let Semester = db.Semester;
let PracticalObjection = db.PracticalObjection;
let SSS = db.SemesterSubjectsStudents;
let PracticalExam = db.PracticalExam;
let ExamResults = db.ExamResults;
let Subject = db.Subject;

exports.create = async (req,res) => {
    let semester = await Semester.findOne({where:{inProgress: true}}), type = req.body.type,resultId = req.body.resultId, model = Objection;
    if (type === "practical")
        model = PracticalObjection;
    model.create({studentId:req.userId,semesterId:semester.id,examResultId:resultId})
        .then(objection => {
            res.send(objection);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message});
        });
}

exports.findAll = (req,res) => {
    Objection.findAll({where:{statusId:1},include:[{all:true},{model:Student,include:Department}]})
        .then(objection => {
            res.send(objection);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message});
        });
}

exports.GetObjectionSubjects = async (req,res) => {
    let type = req.query.type, model = ExamResults;
    if (type ==="practical")
        model = PracticalExam;
    let semester = await Semester.findOne({where:{inProgress: true}});
    console.log(semester.id)
    model.findAll({where:{studentId:req.userId,semesterId:semester.id},include:Subject})
        .then(PE => {
            res.send({data:PE,status:200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message});
        });
};