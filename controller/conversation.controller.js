let db = require("../models");
let Conversation = db.AdminsConversation;
const {Op} = require("sequelize");
let Admin = db.Admin;
let Firebase = require("../controller/firebase.controller");

exports.InsertMessage = async (req,res) => {
    let toAdmin ;
    if (req.roleId ===1||req.roleId===2||req.roleId===5)
        toAdmin = req.body.toAdmin;
    if (req.roleId ===3)
        toAdmin = 1;
    if (req.roleId ===4)
        toAdmin = 2;
    if (req.roleId ===6)
    {
        let ad = Admin.findOne({where:{roleId:5,departmentId:req.departmentId}});
        toAdmin=ad.id;
    }
    let obj ={ toAdmin:toAdmin, fromAdmin: req.userId, text:req.body.text};
    Conversation.create(obj)
        .then(C => {
            Conversation.findAll({where:{[Op.or]:[{fromAdmin:req.userId},{toAdmin:req.userId}]}})
                .then(async C2 =>{
                    let ToAdmin = await Admin.findByPk(toAdmin);
                    let FromAdmin = await Admin.findByPk(req.userId);
                    Firebase.send(ToAdmin.fcmToken,"لقد وصلتك رسالة من "+FromAdmin.username);
                    res.send({data:C2,status:200})
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).send({message:err.message,status:500});
                });
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message,status:500});
        });
};

exports.GetMessages = (req,res) => {
    Conversation.findAll({where:{[Op.or]:[{fromAdmin:req.userId},{toAdmin:req.userId}]}})
        .then(C2 =>{
            res.send({data:C2,status:200})
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({message:err.message,status:500});
        });
};