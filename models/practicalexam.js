'use strict';
module.exports = (sequelize, DataTypes) => {
  const PracticalExam = sequelize.define('PracticalExam', {
    subjectId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    semesterId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    studentId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    mark: {
      type:DataTypes.INTEGER,
      allowNull:false
    }
  }, {});
  PracticalExam.associate = function(models) {
    // associations can be defined here
  };
  return PracticalExam;
};