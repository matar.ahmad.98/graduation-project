'use strict';
module.exports = (sequelize, DataTypes) => {
  const Notification = sequelize.define('Notification', {
    text: {
      type:DataTypes.STRING,
      allowNull:false
    },
    adminId: {
      type:DataTypes.INTEGER,
      allowNull:false
    }
  }, {});
  Notification.associate = function(models) {
    // associations can be defined here
  };
  return Notification;
};