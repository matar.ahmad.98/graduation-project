'use strict';
module.exports = (sequelize, DataTypes) => {
  const Admin = sequelize.define('Admin', {
    username: {
      type:DataTypes.STRING,
      unique:true
    },
    fullName: {
      type:DataTypes.STRING,
    },
    fcmToken: DataTypes.STRING,
    password: DataTypes.STRING,
    isActive: {
      type:DataTypes.BOOLEAN,
      defaultValue: true
    },
    roleId: DataTypes.INTEGER
  }, {});
  Admin.associate = function(models) {
    // associations can be defined here
  };
  return Admin;
};