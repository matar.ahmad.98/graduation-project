'use strict';
module.exports = (sequelize, DataTypes) => {
  const SemesterSubjectsProfessors = sequelize.define('SemesterSubjectsProfessors', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    professorId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    subjectId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    semesterId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    isActive: {
      type:DataTypes.BOOLEAN,
      defaultValue: 1
    }
  }, {});
  SemesterSubjectsProfessors.associate = function(models) {
    // associations can be defined here
  };
  return SemesterSubjectsProfessors;
};