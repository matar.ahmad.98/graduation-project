'use strict';
module.exports = (sequelize, DataTypes) => {
  const Semester = sequelize.define('Semester', {
    yearId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    semesterNumber: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    inProgress: {
      type: DataTypes.BOOLEAN,
      defaultValue:true
    }
  }, {});
  Semester.associate = function(models) {
    // associations can be defined here
  };
  return Semester;
};