'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.js')[env];
const db = {};
let bcrypt = require("bcryptjs");

let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes)
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

console.log(db);
db.sequelize = sequelize;
db.Sequelize = Sequelize;

// Role 1:M Admin
db.Admin.belongsTo(db.Role,{
  foreignKey:'roleId'
});
db.Role.hasMany(db.Admin,{
  foreignKey:'roleId'
});

// Admin 1:M Department
db.Admin.belongsTo(db.Department,{
  foreignKey:'departmentId'
});
db.Department.hasMany(db.Admin,{
  foreignKey:'departmentId'
});

// FReg M:1 OrderStatus
db.FReg.belongsTo(db.OrderStatus,{
  foreignKey:"statusId"
});
db.OrderStatus.hasMany(db.FReg,{
  foreignKey:"statusId"
});
db.FReg.belongsTo(db.Admin,{
  foreignKey:"adminId"
});
db.Admin.hasMany(db.FReg,{
  foreignKey:"adminId"
});
db.FReg.belongsTo(db.Student,{
  foreignKey:"studentId"
});
db.Student.hasMany(db.FReg,{
  foreignKey:"studentId"
});

// AReg M:1 OrderStatus
db.AReg.belongsTo(db.OrderStatus,{
  foreignKey:"statusId"
});
db.OrderStatus.hasMany(db.AReg,{
  foreignKey:"statusId"
});
db.AReg.belongsTo(db.Admin,{
  foreignKey:"adminId"
});
db.Admin.hasMany(db.AReg,{
  foreignKey:"adminId"
});
db.AReg.belongsTo(db.Student,{
  foreignKey:"studentId"
});
db.Student.hasMany(db.AReg,{
  foreignKey:"studentId"
});

// MilitaryDeferment M:1 OrderStatus
db.MilitaryDeferment.belongsTo(db.OrderStatus,{
  foreignKey:"statusId"
});
db.OrderStatus.hasMany(db.MilitaryDeferment,{
  foreignKey:"statusId"
});
db.MilitaryDeferment.belongsTo(db.Admin,{
  foreignKey:"adminId"
});
db.Admin.hasMany(db.MilitaryDeferment,{
  foreignKey:"adminId"
});
db.MilitaryDeferment.belongsTo(db.Student,{
  foreignKey:"studentId"
});
db.Student.hasMany(db.MilitaryDeferment,{
  foreignKey:"studentId"
});


// Objection M:1 OrderStatus
db.Objection.belongsTo(db.OrderStatus,{
  foreignKey:"statusId"
});
db.OrderStatus.hasMany(db.Objection,{
  foreignKey:"statusId"
});
db.Objection.belongsTo(db.Admin,{
  foreignKey:"adminId"
});
db.Admin.hasMany(db.Objection,{
  foreignKey:"adminId"
});
db.Objection.belongsTo(db.Student,{
  foreignKey:"studentId"
});
db.Student.hasMany(db.Objection,{
  foreignKey:"studentId"
});
db.Objection.belongsTo(db.ExamResults,{
  foreignKey:"examResultId"
});
db.ExamResults.hasOne(db.Objection,{
  foreignKey:"examResultId"
});

// StudentCareer M:1 OrderStatus
db.StudentCareer.belongsTo(db.OrderStatus,{
  foreignKey:"statusId"
});
db.OrderStatus.hasMany(db.StudentCareer,{
  foreignKey:"statusId"
});
db.StudentCareer.belongsTo(db.Admin,{
  foreignKey:"adminId"
});
db.Admin.hasMany(db.StudentCareer,{
  foreignKey:"adminId"
});
db.StudentCareer.belongsTo(db.Student,{
  foreignKey:"studentId"
});
db.Student.hasMany(db.StudentCareer,{
  foreignKey:"studentId"
});


// New Relations
// AcademicYear 1:M Semester
db.AcademicYear.hasMany(db.Semester,{
  foreignKey:"yearId"
});
db.Semester.belongsTo(db.AcademicYear,{
  foreignKey:"yearId"
});

// Semester 1:M SemesterSubjectsStudents
db.SemesterSubjectsStudents.belongsTo(db.Semester,{
  foreignKey:"semesterId"
});
db.Semester.hasMany(db.SemesterSubjectsStudents,{
  foreignKey:"semesterId"
});

// Subject 1:M SemesterSubjectsStudents
db.SemesterSubjectsStudents.belongsTo(db.Subject,{
  foreignKey:"subjectId"
});
db.Subject.hasMany(db.SemesterSubjectsStudents,{
  foreignKey:"subjectId"
});

// Subject 1:M SemesterSubjectsStudents
db.SemesterSubjectsStudents.belongsTo(db.Student,{
  foreignKey:"studentId"
});
db.Student.hasMany(db.SemesterSubjectsStudents,{
  foreignKey:"studentId"
});

// Department 1:M Subject
db.Subject.belongsTo(db.Department,{
  foreignKey:"departmentId"
});
db.Department.hasMany(db.Subject,{
  foreignKey:"departmentId"
});

// Semester 1:M SemesterSubjectsProfessors
db.SemesterSubjectsProfessors.belongsTo(db.Semester,{
  foreignKey:"semesterId"
});
db.Semester.hasMany(db.SemesterSubjectsProfessors,{
  foreignKey:"semesterId"
});

// Subject N:M Professor
db.Subject.belongsToMany(db.Professor,{
  through: db.SemesterSubjectsProfessors,
  foreignKey: "subjectId"
});
db.Professor.belongsToMany(db.Subject,{
  through: db.SemesterSubjectsProfessors,
  foreignKey: "professorId"
});
db.SemesterSubjectsProfessors.belongsTo(db.Professor,{
  foreignKey: "professorId"
});
db.SemesterSubjectsProfessors.belongsTo(db.Subject,{
  foreignKey: "subjectId"
});
db.Professor.hasMany(db.SemesterSubjectsProfessors,{
  foreignKey: "professorId"
});
db.Subject.hasMany(db.SemesterSubjectsProfessors,{
  foreignKey: "subjectId"
});

// Subject 1:M SemesterSubjectsStudents
db.SemesterSubjectsProfessors.belongsTo(db.Subject,{
  foreignKey:"subjectId"
});
db.Subject.hasMany(db.SemesterSubjectsProfessors,{
  foreignKey:"subjectId"
});

// Subject 1:M SemesterSubjectsStudents
db.SemesterSubjectsProfessors.belongsTo(db.Professor,{
  foreignKey:"professorId"
});
db.Professor.hasMany(db.SemesterSubjectsProfessors,{
  foreignKey:"professorId"
});

// Professor 1:M Department
db.Professor.belongsTo(db.Department,{
  foreignKey:"departmentId"
});
db.Department.hasMany(db.Professor,{
  foreignKey:"departmentId"
});

// Semester 1:M ExamResults
db.ExamResults.belongsTo(db.Semester,{
  foreignKey:"semesterId"
});
db.Semester.hasMany(db.ExamResults,{
  foreignKey:"semesterId"
});

// Subject 1:M ExamResults
db.ExamResults.belongsTo(db.Subject,{
  foreignKey:"subjectId"
});
db.Subject.hasMany(db.ExamResults,{
  foreignKey:"subjectId"
});

// PracticalExam 1:1 ExamResults
db.ExamResults.belongsTo(db.PracticalExam,{
  foreignKey:"practicalMarkId"
});
db.PracticalExam.hasOne(db.ExamResults,{
  foreignKey:"practicalMarkId"
});

// Admin 1:M ExamResults
db.ExamResults.belongsTo(db.Admin,{
  foreignKey:"adminId"
});
db.Admin.hasMany(db.ExamResults,{
  foreignKey:"adminId"
});

// Admin 1:M ExamResults
db.ExamResults.belongsTo(db.Student,{
  foreignKey:"studentId"
});
db.Student.hasMany(db.ExamResults,{
  foreignKey:"studentId"
});

// Semester 1:M PracticalExam
db.PracticalExam.belongsTo(db.Semester,{
  foreignKey:"semesterId"
});
db.Semester.hasMany(db.PracticalExam,{
  foreignKey:"semesterId"
});

// Subject 1:M PracticalExam
db.PracticalExam.belongsTo(db.Subject,{
  foreignKey:"subjectId"
});
db.Subject.hasMany(db.PracticalExam,{
  foreignKey:"subjectId"
});

db.PracticalExam.belongsTo(db.Student,{
  foreignKey:"studentId"
});
db.Student.hasMany(db.PracticalExam,{
  foreignKey:"studentId"
});

// Admin 1:M ExamResults
db.PracticalExam.belongsTo(db.Admin,{
  foreignKey:"adminId"
});
db.Admin.hasMany(db.PracticalExam,{
  foreignKey:"adminId"
});

// Penalties N:M Student
db.Student.belongsToMany(db.Penalties,{
  through:db.StudentPenalties,
  foreignKey:"studentId"
});
db.Penalties.belongsToMany(db.Student,{
  through:db.StudentPenalties,
  foreignKey:"penaltyId"
});

// Admin 1:M StudentPenalties
db.StudentPenalties.belongsTo(db.Admin,{
  foreignKey:"adminId"
});
db.Admin.hasMany(db.StudentPenalties,{
  foreignKey:"adminId"
});

// Semester 1:M StudentPenalties
db.StudentPenalties.belongsTo(db.Semester,{
  foreignKey:"semesterId"
});
db.Semester.hasMany(db.StudentPenalties,{
  foreignKey:"semesterId"
});

// Penalties 1:M StudentPenalties
db.StudentPenalties.belongsTo(db.Penalties,{
  foreignKey:"penaltyId"
});
db.Penalties.hasMany(db.StudentPenalties,{
  foreignKey:"penaltyId"
});

// Student 1:M StudentPenalties
db.StudentPenalties.belongsTo(db.Student,{
  foreignKey:"studentId"
});
db.Student.hasMany(db.StudentPenalties,{
  foreignKey:"studentId"
});

// Notification 1:M Admin
db.Notification.belongsTo(db.Admin,{
  foreignKey:"adminId"
});
db.Admin.hasMany(db.Notification,{
  foreignKey:"adminId"
});

// RegistrationRequirements 1:M CertificateType
db.RegistrationRequirements.belongsTo(db.CertificateType,{
  foreignKey:"certificateTypeId"
});
db.CertificateType.hasMany(db.RegistrationRequirements,{
  foreignKey:"certificateTypeId"
});

// RegistrationRequirements 1:M CertificateSection
db.RegistrationRequirements.belongsTo(db.CertificateSection,{
  foreignKey:"sectionId"
});
db.CertificateSection.hasMany(db.RegistrationRequirements,{
  foreignKey:"sectionId"
});

// Objection 1:1 ObjectionResult
db.ObjectionResult.belongsTo(db.Objection,{
  foreignKey:"objectionId"
});
db.Objection.hasOne(db.ObjectionResult,{
  foreignKey:"objectionId"
});

// Admin 1:M ObjectionResult
db.ObjectionResult.belongsTo(db.Admin,{
  foreignKey:"adminId"
});
db.Admin.hasMany(db.ObjectionResult,{
  foreignKey:"adminId"
});

// ObjectionNotification 1:M Semester
db.ObjectionNotification.belongsTo(db.Semester,{
  foreignKey:"semesterId"
});
db.Semester.hasMany(db.ObjectionNotification,{
  foreignKey:"semesterId"
});

// PendingStudents 1:1 Department
db.PendingStudent.belongsTo(db.Department,{
  foreignKey:"departmentId"
});
db.Department.hasMany(db.PendingStudent,{
  foreignKey:"departmentId"
});
db.PendingStudent.belongsTo(db.MilitaryDepartment,{
  foreignKey:"militaryDepartmentId"
});
db.MilitaryDepartment.hasMany(db.PendingStudent,{
  foreignKey:"militaryDepartmentId"
});
db.PendingStudent.belongsTo(db.ForeignLanguage,{
  foreignKey:"foreignLanguageId"
});
db.ForeignLanguage.hasMany(db.PendingStudent,{
  foreignKey:"foreignLanguageId"
});

// Students 1:1 Department
db.Student.belongsTo(db.Department,{
  foreignKey:"departmentId"
});
db.Department.hasMany(db.Student,{
  foreignKey:"departmentId"
});
db.Student.belongsTo(db.MilitaryDepartment,{
  foreignKey:"militaryDepartmentId"
});
db.MilitaryDepartment.hasMany(db.Student,{
  foreignKey:"militaryDepartmentId"
});
db.Student.belongsTo(db.ForeignLanguage,{
  foreignKey:"foreignLanguageId"
});
db.ForeignLanguage.hasMany(db.Student,{
  foreignKey:"foreignLanguageId"
});
db.AcademicYear.hasMany(db.AReg,{
  foreignKey: "yearId"
});
db.AReg.belongsTo(db.AcademicYear,{
  foreignKey: "yearId"
});
db.PracticalObjection.belongsTo(db.PracticalExam,{
  foreignKey: "examResultId"
});
db.PracticalExam.hasOne(db.PracticalObjection,{
  foreignKey: "examResultId"
});
db.PracticalObjection.belongsTo(db.Semester,{
  foreignKey: "semesterId"
});
db.Semester.hasMany(db.PracticalObjection,{
  foreignKey: "semesterId"
});
db.PracticalObjection.belongsTo(db.OrderStatus,{
  foreignKey: "statusId"
});
db.OrderStatus.hasMany(db.PracticalObjection,{
  foreignKey: "statusId"
});
db.PracticalObjection.belongsTo(db.Admin,{
  foreignKey: "adminId"
});
db.Admin.hasMany(db.PracticalObjection,{
  foreignKey: "adminId"
});
db.PracticalObjection.belongsTo(db.Student,{
  foreignKey: "studentId"
});
db.Student.hasMany(db.PracticalObjection,{
  foreignKey: "studentId"
});
db.PracticalObjection.hasOne(db.PracticalObjectionResult,{
  foreignKey: "practicalObjectionId"
});
db.PracticalObjectionResult.belongsTo(db.PracticalObjection,{
  foreignKey: "practicalObjectionId"
});
db.AdminsConversation.belongsTo(db.Admin,{
  foreignKey: "fromAdmin"
});
db.Admin.hasMany(db.AdminsConversation,{
  foreignKey: "fromAdmin"
});
db.AdminsConversation.belongsTo(db.Admin,{
  foreignKey: "toAdmin"
});
db.Admin.hasMany(db.AdminsConversation,{
  foreignKey: "toAdmin"
});

db.Role.create({
  id:1,
  role:"Affairs-SuperAdmin",
  arRole:"رئيس قسم شؤون الطلاب"
});
db.Role.create({
  id:2,
  role:"Examination-SuperAdmin",
  arRole:"رئيس قسم الامتحانات"
});
db.Role.create({
  id:3,
  role:"Affairs-Admin",
  arRole: "موظف شؤون الطلاب"
});
db.Role.create({
  id:4,
  role:"Examination-Admin",
  arRole:"موظف امتحانات"
});
db.Role.create({
  id:5,
  role:"Head-Of-Department",
  arRole:"رئيس قسم",
});
db.Role.create({
  id:6,
  role:"Department-Employee",
  arRole:"موظف قسم",
});

db.OrderStatus.create({
  id:1,
  status:"pending"
});
db.OrderStatus.create({
  id:2,
  status:"approved"
});
db.OrderStatus.create({
  id:3,
  status:"rejected"
});
db.OrderStatus.create({
  id:4,
  status:"in progress"
});
db.OrderStatus.create({
  id:5,
  status:"Examination Approved"
});

db.Admin.create({
  id:1,
  username:"affSuperAdmin",
  fullName:"تيسير الجاسم",
  password:bcrypt.hashSync("password", 8),
  isActive:true,
  roleId:1
});
db.Admin.create({
  id:2,
  username:"exaSuperAdmin",
  fullName:"احمد محمد",
  password:bcrypt.hashSync("password", 8),
  isActive:true,
  roleId:2
});
db.Admin.create({
  id:3,
  username:"affAdmin",
  fullName:"خالد حمدي",
  password:bcrypt.hashSync("password", 8),
  isActive:true,
  roleId:3
});
db.Admin.create({
  id:4,
  username:"exaAdmin",
  departmentId:1,
  fullName:"حمدي خالد",
  password:bcrypt.hashSync("password", 8),
  isActive:true,
  roleId:4
});
db.Admin.create({
  id:5,
  username:"HeadOfDepartment",
  fullName:"محمد عبد الوهاب",
  password:bcrypt.hashSync("password", 8),
  departmentId: 1,
  isActive:true,
  roleId:5
});
db.Admin.create({
  id:6,
  username:"EmployeeDepartment",
  fullName:"عبد الوهاب محمد",
  password:bcrypt.hashSync("password", 8),
  departmentId: 1,
  isActive:true,
  roleId:6
});
db.Department.create({
  id:1,
  name:"قسم هندسة الحواسيب والاتمتة"
});
db.Department.create({
  id:2,
  name:"قسم هندسة الاتصالات والالكترون"
});
db.Department.create({
  id:3,
  name:"قسم هندسة الطاقة الكهرباية"
});
db.Department.create({
  id:4,
  name:"قسم الميكانيك العام"
});
db.Professor.create({
  id:1,
  name:"يزن",
  departmentId:1
});
db.Professor.create({
  id:2,
  name:"محمد",
  departmentId:1
});
db.Professor.create({
  id:3,
  name:"محمود",
  departmentId:1
});
db.Professor.create({
  id:4,
  name:"علي",
  departmentId:1
});
db.Professor.create({
  id:5,
  name:"احمد",
  departmentId:1
});
db.Professor.create({
  id:6,
  name:"يعفوب",
  departmentId:1
});
db.Professor.create({
  id:7,
  name:"موسر",
  departmentId:1
});
db.Professor.create({
  id:8,
  name:"ابراهيم",
  departmentId:1
});

db.Subject.create({
  id:1,
  name: "تحليل1",
  departmentId: 1,
  semesterNumber: 1,
  yearNumber: 1
});
db.Subject.create({
  id:2,
  name: "فيزيا1",
  departmentId: 1,
  semesterNumber: 2,
  yearNumber: 1
});
db.Subject.create({
  id:3,
  name: "حقول",
  departmentId: 1,
  semesterNumber: 1,
  yearNumber: 2
});
db.Subject.create({
  id:8,
  name: "خوارزميات",
  departmentId: 1,
  semesterNumber: 2,
  yearNumber: 2
});
db.Subject.create({
  id:9,
  name: "تحكم1",
  departmentId: 1,
  semesterNumber: 1,
  yearNumber: 3
});
db.Subject.create({
  id:10,
  name: "تحليل نظم",
  departmentId: 1,
  semesterNumber: 2,
  yearNumber: 3
});
db.Subject.create({
  id:4,
  name: "ذكاء صنعي",
  departmentId: 1,
  semesterNumber: 1,
  yearNumber: 4
});
db.Subject.create({
  id:5,
  name: "تحكم عائم",
  departmentId: 1,
  semesterNumber: 2,
  yearNumber: 4
});
db.Subject.create({
  id:6,
  name: "روبوتية",
  departmentId: 1,
  semesterNumber: 1,
  yearNumber: 5
});
db.Subject.create({
  id:7,
  name: "شبكات عصبونية",
  departmentId: 1,
  semesterNumber: 2,
  yearNumber: 5
});



db.Subject.create({
  id:11,
  name: "رياضيات 1",
  departmentId: 2,
  semesterNumber: 1,
  yearNumber: 1
});
db.Subject.create({
  id:12,
  name: "فيزيا2",
  departmentId: 2,
  semesterNumber: 2,
  yearNumber: 1
});
db.Subject.create({
  id:13,
  name: "حقول",
  departmentId: 2,
  semesterNumber: 1,
  yearNumber: 2
});
db.Subject.create({
  id:14,
  name: "قياسات كهربائية",
  departmentId: 2,
  semesterNumber: 2,
  yearNumber: 2
});
db.Subject.create({
  id:15,
  name: "دارات الكترونيى1",
  departmentId: 2,
  semesterNumber: 1,
  yearNumber: 3
});
db.Subject.create({
  id:16,
  name: "دارات الكترونية2",
  departmentId: 2,
  semesterNumber: 2,
  yearNumber: 3
});
db.Subject.create({
  id:17,
  name: "معالجة اشارة",
  departmentId: 2,
  semesterNumber: 1,
  yearNumber: 4
});
db.Subject.create({
  id:18,
  name: "ترميز",
  departmentId: 2,
  semesterNumber: 2,
  yearNumber: 4
});
db.Subject.create({
  id:19,
  name: "معالجة اشارة2",
  departmentId: 2,
  semesterNumber: 1,
  yearNumber: 5
});
db.Subject.create({
  id:20,
  name: "اقتصاد هندسي",
  departmentId: 2,
  semesterNumber: 2,
  yearNumber: 5
});


db.Subject.create({
  id:21,
  name: "رياضيات 1",
  departmentId: 3,
  semesterNumber: 1,
  yearNumber: 1
});
db.Subject.create({
  id:22,
  name: "فيزيا2",
  departmentId: 3,
  semesterNumber: 2,
  yearNumber: 1
});
db.Subject.create({
  id:23,
  name: "رسم",
  departmentId: 3,
  semesterNumber: 1,
  yearNumber: 2
});
db.Subject.create({
  id:24,
  name: "دارات كهربائية1",
  departmentId: 3,
  semesterNumber: 2,
  yearNumber: 2
});
db.Subject.create({
  id:25,
  name: "دارات كهربائية2",
  departmentId: 3,
  semesterNumber: 1,
  yearNumber: 3
});
db.Subject.create({
  id:26,
  name: "دارات الكترونية2",
  departmentId: 3,
  semesterNumber: 2,
  yearNumber: 3
});
db.Subject.create({
  id:27,
  name: "توتر",
  departmentId: 3,
  semesterNumber: 1,
  yearNumber: 4
});
db.Subject.create({
  id:28,
  name: "بناء محطات",
  departmentId: 3,
  semesterNumber: 2,
  yearNumber: 4
});
db.Subject.create({
  id:29,
  name: "طاقات بديلة",
  departmentId: 3,
  semesterNumber: 1,
  yearNumber: 5
});
db.Subject.create({
  id:30,
  name: "اقتصاد هندسي",
  departmentId: 3,
  semesterNumber: 2,
  yearNumber: 5
});


db.Subject.create({
  id:31,
  name: "رياضيات 1",
  departmentId: 4,
  semesterNumber: 1,
  yearNumber: 1
});
db.Subject.create({
  id:32,
  name: "فيزيا2",
  departmentId: 4,
  semesterNumber: 2,
  yearNumber: 1
});
db.Subject.create({
  id:33,
  name: "رياضايات2",
  departmentId: 4,
  semesterNumber: 1,
  yearNumber: 2
});
db.Subject.create({
  id:34,
  name: "رسم",
  departmentId: 4,
  semesterNumber: 2,
  yearNumber: 2
});
db.Subject.create({
  id:35,
  name: "برمجة",
  departmentId: 4,
  semesterNumber: 1,
  yearNumber: 3
});
db.Subject.create({
  id:36,
  name: "ميكانيك سوائل",
  departmentId: 4,
  semesterNumber: 2,
  yearNumber: 3
});
db.Subject.create({
  id:37,
  name: "ميكانيك سؤائل2",
  departmentId: 4,
  semesterNumber: 1,
  yearNumber: 4
});
db.Subject.create({
  id:38,
  name: "حركة",
  departmentId: 4,
  semesterNumber: 2,
  yearNumber: 4
});
db.Subject.create({
  id:39,
  name: "تحريك هندسي",
  departmentId: 4,
  semesterNumber: 1,
  yearNumber: 5
});
db.Subject.create({
  id:40,
  name: "اقتصاد هندسي",
  departmentId: 4,
  semesterNumber: 2,
  yearNumber: 5
});



db.MilitaryDepartment.create({
  id: 1,
  name: "ميدان"
});
db.MilitaryDepartment.create({
  id: 2,
  name: "صالحية"
});
db.MilitaryDepartment.create({
  id: 3,
  name: "ساروجة"
});
db.MilitaryDepartment.create({
  id: 4,
  name: "وسيطة"
});

db.ForeignLanguage.create({
  id: 1,
  name: "انكليزي"
});
db.ForeignLanguage.create({
  id: 2,
  name: "فرنسي"
});

db.AcademicYear.create({
  id: 1,
  year: "2021",
});
db.Semester.create({
  id: 1,
  yearId:1,
  semesterNumber:1
});
db.Semester.create({
  id: 2,
  yearId:1,
  semesterNumber:2
});
module.exports = db;
