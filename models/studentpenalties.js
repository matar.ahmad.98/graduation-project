'use strict';
module.exports = (sequelize, DataTypes) => {
  const StudentPenalties = sequelize.define('StudentPenalties', {
    studentId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    penaltyId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    semesterId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    adminId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    punishmentEndDate: {
      type:DataTypes.DATE,
      allowNull:false
    },
    punishmentDate: {
      type:DataTypes.DATE,
      allowNull:false
    },
    description: {
      type:DataTypes.STRING,
      allowNull:true
    }
  }, {});
  StudentPenalties.associate = function(models) {
    // associations can be defined here
  };
  return StudentPenalties;
};