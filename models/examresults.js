'use strict';
module.exports = (sequelize, DataTypes) => {
  const ExamResults = sequelize.define('ExamResults', {
    subjectId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    semesterId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    studentId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    mark:  {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    practicalMarkId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    adminId: {
      type:DataTypes.INTEGER,
      allowNull:false
    }
  }, {});
  ExamResults.associate = function(models) {
    // associations can be defined here
  };
  return ExamResults;
};