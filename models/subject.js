'use strict';
module.exports = (sequelize, DataTypes) => {
  const Subject = sequelize.define('Subject', {
    name: {
      type:DataTypes.STRING,
      allowNull:false
    },
    departmentId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    semesterNumber: {
      type:DataTypes.INTEGER,
      allowNull: false,
      defaultValue:1
    },
    yearNumber: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    practicalExamMaxMark: {
      type:DataTypes.INTEGER,
      allowNull: false,
      defaultValue:30
    },
    finalExamMaxMark: {
      type:DataTypes.INTEGER,
      allowNull: false,
      defaultValue:70
    },
  }, {});
  Subject.associate = function(models) {
    // associations can be defined here
  };
  return Subject;
};