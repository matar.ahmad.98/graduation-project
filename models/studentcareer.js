'use strict';
module.exports = (sequelize, DataTypes) => {
  const StudentCareer = sequelize.define('StudentCareer', {
    statusId: {
      type:DataTypes.INTEGER,
      defaultValue:1
    },
    adminId: DataTypes.INTEGER,
    studentId: DataTypes.INTEGER,
    image: DataTypes.STRING
  }, {});
  StudentCareer.associate = function(models) {
    // associations can be defined here
  };
  return StudentCareer;
};