'use strict';
module.exports = (sequelize, DataTypes) => {
  const PendingStudent = sequelize.define('PendingStudent', {
    firstName: {
      type:DataTypes.STRING,
      allowNull:false
    },
    lastName: {
      type:DataTypes.STRING,
      allowNull:false
    },
    username: {
      type:DataTypes.STRING,
      allowNull:false
    },
    phoneNumber: {
      type:DataTypes.STRING,
      allowNull:false
    },
    email: {
      type:DataTypes.STRING,
      allowNull:false
    },
    password: {
      type:DataTypes.STRING,
      allowNull:false
    },
    isActive: {
      type:DataTypes.BOOLEAN,
      defaultValue:true
    },
    departmentId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    militaryDepartmentId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    foreignLanguageId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    stateName: {
      type:DataTypes.STRING,
      allowNull:true
    },
    housingNumber: {
      type:DataTypes.STRING,
      allowNull:false
    },
    FrontID: {
      type:DataTypes.STRING,
      allowNull:false
    },
    BackID: {
      type:DataTypes.STRING,
      allowNull:false
    },
    Certificate: {
      type:DataTypes.STRING,
      allowNull:false
    },
    PersonalPic: {
      type:DataTypes.STRING,
      allowNull:false
    }

  }, {});
  PendingStudent.associate = function(models) {
    // associations can be defined here
  };
  return PendingStudent;
};