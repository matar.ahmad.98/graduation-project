'use strict';
module.exports = (sequelize, DataTypes) => {
  const CertificateType = sequelize.define('CertificateType', {
    name: {
      type:DataTypes.STRING,
      allowNull:false
    }
  }, {});
  CertificateType.associate = function(models) {
    // associations can be defined here
  };
  return CertificateType;
};