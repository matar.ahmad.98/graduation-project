'use strict';
module.exports = (sequelize, DataTypes) => {
  const SemesterSubjectsStudents = sequelize.define('SemesterSubjectsStudents', {
    studentId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    subjectId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    semesterId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    isSucceeded: {
      type:DataTypes.BOOLEAN,
      defaultValue:false
    }
  }, {});
  SemesterSubjectsStudents.associate = function(models) {
    // associations can be defined here
  };
  return SemesterSubjectsStudents;
};