'use strict';
module.exports = (sequelize, DataTypes) => {
  const ObjectionNotification = sequelize.define('ObjectionNotification', {
    semesterId:{
      type:DataTypes.INTEGER,
      allowNull:false
    },
    startDate:{
      type:DataTypes.DATE,
      allowNull:false
    },
    endDate:{
      type:DataTypes.DATE,
      allowNull:false
    },
  }, {});
  ObjectionNotification.associate = function(models) {
    // associations can be defined here
  };
  return ObjectionNotification;
};