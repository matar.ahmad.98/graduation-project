'use strict';
module.exports = (sequelize, DataTypes) => {
  const FReg = sequelize.define('FReg', {
    statusId: DataTypes.INTEGER,
    adminId: DataTypes.INTEGER,
    studentId: DataTypes.INTEGER,
    PersonalPic:DataTypes.STRING,
    Bach:DataTypes.STRING,
    IdCopy:DataTypes.STRING
  }, {});
  FReg.associate = function(models) {
    // associations can be defined here
  };
  return FReg;
};