'use strict';
module.exports = (sequelize, DataTypes) => {
    const MilitaryDepartment = sequelize.define('MilitaryDepartment', {
        name: DataTypes.STRING,
    }, {});
    MilitaryDepartment.associate = function(models) {
        // associations can be defined here
    };
    return MilitaryDepartment;
};