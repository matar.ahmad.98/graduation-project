'use strict';
module.exports = (sequelize, DataTypes) => {
  const ObjectionResult = sequelize.define('ObjectionResult', {
    objectionId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    isChange: {
      type:DataTypes.BOOLEAN,
      allowNull:false
    },
    finalMark: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    originalMark: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    adminId: {
      type:DataTypes.INTEGER
    }
  }, {});
  ObjectionResult.associate = function(models) {
    // associations can be defined here
  };
  return ObjectionResult;
};