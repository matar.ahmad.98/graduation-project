'use strict';
module.exports = (sequelize, DataTypes) => {
  const PracticalObjectionResult = sequelize.define('PracticalObjectionResult', {
    practicalObjectionId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    isChange: {
      type:DataTypes.BOOLEAN,
      allowNull:false
    },
    finalMark: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    originalMark: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    adminId: {
      type:DataTypes.INTEGER
    }
  }, {});
  PracticalObjectionResult.associate = function(models) {
    // associations can be defined here
  };
  return PracticalObjectionResult;
};