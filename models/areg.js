'use strict';
module.exports = (sequelize, DataTypes) => {
  const AReg = sequelize.define('AReg', {
    statusId: {
      type:DataTypes.INTEGER,
      defaultValue:1
    },
    adminId: DataTypes.INTEGER,
    studentId: DataTypes.INTEGER,
    BackID: DataTypes.STRING,
    FrontID: DataTypes.STRING,
    yearId: {
      type:DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    }
  }, {});
  AReg.associate = function(models) {
    // associations can be defined here
  };
  return AReg;
};