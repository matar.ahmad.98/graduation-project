'use strict';
module.exports = (sequelize, DataTypes) => {
  const CertificateSection = sequelize.define('CertificateSection', {
    name: {
      type:DataTypes.STRING,
      allowNull:false
    }
  }, {});
  CertificateSection.associate = function(models) {
    // associations can be defined here
  };
  return CertificateSection;
};