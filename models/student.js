'use strict';
module.exports = (sequelize, DataTypes) => {
  const Student = sequelize.define('Student', {
    firstName: {
      type:DataTypes.STRING,
      allowNull:false
    },
    lastName: {
      type:DataTypes.STRING,
      allowNull:false
    },
    username: {
      type:DataTypes.STRING,
      allowNull:false
    },
      phoneNumber: {
      type:DataTypes.STRING,
      allowNull:false
    },
    email: {
      type:DataTypes.STRING,
      allowNull:false
    },
    password: {
      type:DataTypes.STRING,
      allowNull:false
    },
    isActive: {
      type:DataTypes.BOOLEAN,
      defaultValue: true
    },
    departmentId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    militaryDepartmentId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    foreignLanguageId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    stateName: {
      type:DataTypes.STRING,
      allowNull:true
    },
    housingNumber: {
      type:DataTypes.STRING,
      allowNull:true
    },
    isFallen: {
      type:DataTypes.BOOLEAN,
      defaultValue: false
    },
    currentYear:{
      type:DataTypes.INTEGER,
      defaultValue:1
    },
    FrontID: {
      type:DataTypes.STRING,
    },
    BackID: {
      type:DataTypes.STRING,
    },
    Certificate: {
      type:DataTypes.STRING,
    },
    PersonalPic: {
      type:DataTypes.STRING,
    },
    fcmToken: {
      type:DataTypes.STRING,

    }
  }, {});
  Student.associate = function(models) {
    // associations can be defined here
  };
  return Student;
};