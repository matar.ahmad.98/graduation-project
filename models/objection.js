'use strict';
module.exports = (sequelize, DataTypes) => {
  const Objection = sequelize.define('Objection', {
    examResultId:DataTypes.INTEGER,
    semesterId:DataTypes.INTEGER,
    statusId: {
      type:DataTypes.INTEGER,
      defaultValue:1
    },
    adminId: DataTypes.INTEGER,
    studentId: DataTypes.INTEGER,
    image: DataTypes.STRING
  }, {});
  Objection.associate = function(models) {
    // associations can be defined here
  };
  return Objection;
};