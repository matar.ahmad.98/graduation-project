'use strict';
module.exports = (sequelize, DataTypes) => {
    const PracticalObjection = sequelize.define('PracticalObjection', {
        examResultId:DataTypes.INTEGER,
        semesterId:DataTypes.INTEGER,
        statusId: {
            type:DataTypes.INTEGER,
            defaultValue:1
        },
        adminId: DataTypes.INTEGER,
        studentId: DataTypes.INTEGER,
    }, {});
    PracticalObjection.associate = function(models) {
        // associations can be defined here
    };
    return PracticalObjection;
};