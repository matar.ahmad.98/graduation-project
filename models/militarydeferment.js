'use strict';
module.exports = (sequelize, DataTypes) => {
  const MilitaryDeferment = sequelize.define('MilitaryDeferment', {
    statusId: {
      type:DataTypes.INTEGER,
    defaultValue:1
    },
    adminId: DataTypes.INTEGER,
    studentId: DataTypes.INTEGER,
  }, {});
  MilitaryDeferment.associate = function(models) {
    // associations can be defined here
  };
  return MilitaryDeferment;
};