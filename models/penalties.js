'use strict';
module.exports = (sequelize, DataTypes) => {
  const Penalties = sequelize.define('Penalties', {
    name: {
      type:DataTypes.STRING,
      allowNull:false
    }
  }, {});
  Penalties.associate = function(models) {
    // associations can be defined here
  };
  return Penalties;
};