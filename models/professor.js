'use strict';
module.exports = (sequelize, DataTypes) => {
  const Professor = sequelize.define('Professor', {
    name: {
      type:DataTypes.STRING,
      allowNull:false
    },
    departmentId: {
      type:DataTypes.INTEGER,
      allowNull:false
    }
  }, {});
  Professor.associate = function(models) {
    // associations can be defined here
  };
  return Professor;
};