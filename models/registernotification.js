'use strict';
module.exports = (sequelize, DataTypes) => {
  const RegisterNotification = sequelize.define('RegisterNotification', {
    semesterId:{
      type:DataTypes.INTEGER,
      allowNull:false
    },
    startDate:{
      type:DataTypes.DATE,
      allowNull:false
    },
    endDate:{
      type:DataTypes.DATE,
      allowNull:false
    },
  }, {});
  RegisterNotification.associate = function(models) {
    // associations can be defined here
  };
  return RegisterNotification;
};