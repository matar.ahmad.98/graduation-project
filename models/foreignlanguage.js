'use strict';
module.exports = (sequelize, DataTypes) => {
    const ForeignLanguage = sequelize.define('ForeignLanguage', {
        name: DataTypes.STRING,
    }, {});
    ForeignLanguage.associate = function(models) {
        // associations can be defined here
    };
    return ForeignLanguage;
};