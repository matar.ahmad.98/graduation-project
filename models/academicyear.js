'use strict';
module.exports = (sequelize, DataTypes) => {
  const AcademicYear = sequelize.define('AcademicYear', {
    year: {
      type:DataTypes.STRING,
      allowNull:false
    }
  }, {});
  AcademicYear.associate = function(models) {
    // associations can be defined here
  };
  return AcademicYear;
};