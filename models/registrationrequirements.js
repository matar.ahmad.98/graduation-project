'use strict';
module.exports = (sequelize, DataTypes) => {
  const RegistrationRequirements = sequelize.define('RegistrationRequirements', {
    studentId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    sectionId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    certificateTypeId: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    certificateAverage: {
      type:DataTypes.INTEGER,
      allowNull:false
    },
    certificateDate : {
      type:DataTypes.DATE,
      allowNull:false
    },
    RegistrationDate: {
      type:DataTypes.DATE,
      allowNull:false
    },
  }, {});
  RegistrationRequirements.associate = function(models) {
    // associations can be defined here
  };
  return RegistrationRequirements;
};