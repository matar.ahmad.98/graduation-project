'use strict';
module.exports = (sequelize, DataTypes) => {
    const AdminsConversation = sequelize.define('AdminsConversation', {
        fromAdmin: {
            type:DataTypes.INTEGER,
            allowNull: false
        },
        toAdmin: {
            type:DataTypes.INTEGER,
            allowNull: false
        },
        text: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {});
    AdminsConversation.associate = function(models) {
        // associations can be defined here
    };
    return AdminsConversation;
};